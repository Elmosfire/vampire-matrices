# Vampire matrices

In this [video](https://youtu.be/9nogAYHmnNw?t=543) Matt Parker states at the timestamp 9:03 that he does not think anyone ever found some 3x3 vampire matrices with distinct fangs. So I decided to generate a few of them, you can find them below. The algoritme to produce them can be found in main.rs

The full list of vampire matrices is to large to fit here so it can be found here https://gitlab.com/Elmosfire/vampire-matrices/-/blob/master/output/. Each page here contains 50 elelments,, of the total 6441 elements.

An explanation of the algoritm can be found here: https://elmusfire.blogspot.com/2020/11/vampire-matrices-algoritm.html

