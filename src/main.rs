use std::fmt;
use std::collections::HashSet;
use std::collections::HashMap;
use std::ops;
use std::fs::File;
use std::io::{BufWriter, Write};
use indicatif::ProgressBar;

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
struct Triple {
    x0:u16,
    x1:u16,
    x2:u16
}

impl Triple {
    fn get(&self, index: u8) -> u16 {
        match index{
            0 => self.x0,
            1 => self.x1,
            2 => self.x2,
            _ => panic!("invalid index")
        }
    }
    fn valid(&self) -> bool {
        (self.x0 != self.x1) && (self.x0 != self.x2) && (self.x2 != self.x1)
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
struct LocalTriple {
    triple: Triple,
    index: u8
}

struct TripleGen {
    current: Triple,
    singlemax:u16
}

impl Iterator for TripleGen {
    type Item = Triple;

    // Here, we define the sequence using `.curr` and `.next`.
    // The return type is `Option<T>`:
    //     * When the `Iterator` is finished, `None` is returned.
    //     * Otherwise, the next value is wrapped in `Some` and returned.
    fn next(&mut self) -> Option<Triple> {

        if self.current.x2 < self.singlemax-1 {
            self.current.x2 += 1;
        }
        else if self.current.x1 < self.singlemax-1 {
            self.current.x2 = 1;
            self.current.x1 += 1;
        }
        else if self.current.x0 < self.singlemax-1 {
            self.current.x2 = 1;
            self.current.x1 = 1;
            self.current.x0 += 1;
        }
        else {
            //println!("finished");
            return None;
        }

        return Some(self.current);
    }
}

fn triples(singlemax: u16) -> TripleGen {
    TripleGen { current: Triple{ x0: 1, x1: 1, x2: 1},  singlemax }
}

fn cross(a: Triple, x: Triple) -> u16 {
    a.x0 *x.x0 + a.x1 *x.x1 + a.x2 *x.x2
}

fn res(a: LocalTriple, x: LocalTriple, base:u16) -> u16 {
    a.triple.get(x.index) * base + x.triple.get(a.index)
}

fn resright(a: LocalTriple, x: LocalTriple, base:u16) -> u16 {
    a.triple.get(x.index)  + x.triple.get(a.index) * base
}

fn check_con(a: Triple, x: Triple, base:u16 ) -> bool {
    let val = cross(a,x);
    let digit = val / base;
    let core = val % base;
    let core_valid  = ( core==a.x0) | ( core==a.x1) | ( core==a.x2);
    let digit_valid = (digit==x.x0) | (digit==x.x1) | (digit==x.x2);
    return core_valid & digit_valid
}

fn check_con_local(a: LocalTriple, x: LocalTriple, base:u16 ) -> bool {
    return (cross(a.triple,x.triple) == res(a,x, base)) || (cross(a.triple,x.triple) == resright(a,x, base));
}

impl fmt::Display for Triple {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}, {}]", self.x0, self.x1, self.x2)
    }
}

impl fmt::Display for LocalTriple {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}", self.index, self.triple)
    }
}

struct LocalTripleRes {
    r0: HashSet<LocalTriple>,
    r1: HashSet<LocalTriple>,
    r2: HashSet<LocalTriple>,
}

fn build_right_set(bra: LocalTriple, rightindex: u8, base:u16) -> HashSet<LocalTriple> {
    let mut resset: HashSet<LocalTriple>  = HashSet::new();
    for x in triples(base) {
        let ket = LocalTriple { triple: x, index: rightindex};
        if check_con_local(bra, ket, base) {
            resset.insert(ket);
        }
    }
    return resset;
}

fn get_right_set(bra: LocalTriple, base:u16) -> LocalTripleRes {
    LocalTripleRes {
        r0: build_right_set(bra,0, base),
        r1: build_right_set(bra,1, base),
        r2: build_right_set(bra,2, base)
    }
}

fn build_hm(index: u8,base: u16, max: u16) -> HashMap<LocalTriple, LocalTripleRes> {
    let mut hs:HashMap<LocalTriple, LocalTripleRes> = HashMap::new();
    for x in triples(max) {
        let x0 = LocalTriple { triple: x, index: index};
        let rightset = get_right_set(x0,base);
        if (!rightset.r0.is_empty()) && (!rightset.r1.is_empty()) && (!rightset.r2.is_empty()) {
            hs.insert(x0, rightset);
        }
    }
    return hs
}



impl ops::Add<LocalTripleRes> for LocalTripleRes {
    type Output = LocalTripleRes;

    fn add(self, _rhs: LocalTripleRes) -> LocalTripleRes {
        LocalTripleRes {
            r0: &self.r0 & &_rhs.r0,
            r1: &self.r1 & &_rhs.r1,
            r2: &self.r2 & &_rhs.r2,
        }
    }
}

fn combine_ltr(l:&LocalTripleRes, r: &LocalTripleRes )-> LocalTripleRes  {
    LocalTripleRes {
        r0: &l.r0 & &r.r0,
        r1: &l.r1 & &r.r1,
        r2: &l.r2 & &r.r2,
    }
}

fn check(r0:LocalTripleRes,r1:LocalTripleRes,r2:LocalTripleRes) -> LocalTripleRes {
    return r0+r1+r2
}

fn print_all(base:u16, max:u16) -> std::io::Result<()> {
    let mut file = File::create("output_mixed.txt")?;
    let mut writer = BufWriter::new(&file);
    let s0 = build_hm(0,base,max);
    let s1 = build_hm(1,base,max);
    let s2 = build_hm(2,base,max);
    println!("done initialising");
    for (k1,v1) in &s0 {
        for (k2,v2) in &s1 {
            for (k3,v3) in &s2 {
                let q = combine_ltr(&v1,&combine_ltr(&v2,&v3));
                for right1 in &q.r0 {
                    for right2 in &q.r1 {
                        for right3 in &q.r2 {
                            writeln!(&mut writer, "{};{};{}|{};{};{}", k1, k2, k3, right1, right2, right3);

                        }
                    }
                }
            }
            writer.flush();
        }
    }
    Ok(())
}

fn main() {
    print_all(10,10);

}