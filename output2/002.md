```math
\begin{bmatrix}
  6 & 7\\
  5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 5\\
  5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  77 & 65\\
  65 & 55\\
\end{bmatrix}
```
