```math
\begin{bmatrix}
  3 & 3 & 2\\
  8 & 7 & 6\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 3\\
  6 & 8 & 2\\
  2 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 32 & 23\\
  86 & 78 & 62\\
  22 & 21 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 8\\
  2 & 1 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 8\\
  6 & 3 & 4\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 11 & 68\\
  26 & 13 & 84\\
  22 & 11 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 8\\
  3 & 1 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  8 & 3 & 2\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 11 & 64\\
  28 & 13 & 82\\
  31 & 11 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  4 & 1 & 6\\
  3 & 1 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  1 & 3 & 9\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 11 & 63\\
  41 & 13 & 69\\
  32 & 11 & 58\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  5 & 1 & 5\\
  6 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  5 & 3 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 11 & 61\\
  55 & 13 & 55\\
  61 & 11 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 6\\
  3 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 3\\
  7 & 2 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 12 & 63\\
  27 & 12 & 63\\
  31 & 11 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 6\\
  2 & 1 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 6\\
  4 & 2 & 6\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 12 & 66\\
  24 & 12 & 66\\
  22 & 11 & 58\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 6\\
  4 & 2 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 3\\
  4 & 2 & 3\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 12 & 63\\
  24 & 12 & 63\\
  42 & 21 & 99\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 4\\
  4 & 2 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 2\\
  2 & 1 & 4\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 13 & 62\\
  22 & 11 & 44\\
  42 & 21 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 4\\
  2 & 1 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 4\\
  2 & 1 & 8\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 13 & 64\\
  22 & 11 & 48\\
  22 & 11 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 4\\
  3 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 2\\
  6 & 1 & 4\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 13 & 62\\
  26 & 11 & 44\\
  31 & 11 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 6\\
  4 & 8 & 9\\
  1 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 6\\
  1 & 9 & 3\\
  1 & 1 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 82 & 66\\
  41 & 89 & 93\\
  11 & 31 & 25\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 6\\
  3 & 5 & 8\\
  1 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 8\\
  1 & 9 & 2\\
  1 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 82 & 68\\
  31 & 59 & 82\\
  11 & 31 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 6\\
  3 & 5 & 8\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 4\\
  1 & 9 & 1\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 82 & 64\\
  31 & 59 & 81\\
  21 & 61 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 6\\
  1 & 3 & 2\\
  3 & 8 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 8 & 2\\
  1 & 3 & 2\\
  1 & 8 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 88 & 62\\
  11 & 33 & 22\\
  31 & 88 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  4 & 3 & 6\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 3\\
  2 & 6 & 6\\
  2 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 33 & 63\\
  42 & 36 & 66\\
  22 & 21 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 1 & 2\\
  2 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 3\\
  2 & 4 & 4\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 33 & 63\\
  22 & 14 & 24\\
  21 & 12 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 2 & 3\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 3\\
  2 & 2 & 6\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 36 & 63\\
  22 & 22 & 36\\
  22 & 22 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 2 & 4\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 3\\
  4 & 4 & 2\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 36 & 63\\
  24 & 24 & 42\\
  21 & 21 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 2 & 4\\
  4 & 5 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 6 & 3\\
  2 & 4 & 2\\
  3 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 36 & 63\\
  22 & 24 & 42\\
  43 & 51 & 78\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 2 & 4\\
  3 & 3 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 2 & 4\\
  3 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 33 & 66\\
  22 & 22 & 44\\
  33 & 33 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 2 & 4\\
  4 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 3\\
  4 & 2 & 2\\
  1 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 33 & 63\\
  24 & 22 & 42\\
  41 & 33 & 58\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  3 & 2 & 5\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 9\\
  2 & 6 & 8\\
  2 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 33 & 69\\
  32 & 26 & 58\\
  12 & 11 & 23\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  4 & 4 & 8\\
  4 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 3\\
  4 & 4 & 4\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 33 & 63\\
  44 & 44 & 84\\
  42 & 42 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  4 & 4 & 8\\
  2 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 6\\
  4 & 4 & 8\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 33 & 66\\
  44 & 44 & 88\\
  22 & 22 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  1 & 1 & 2\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 6\\
  2 & 2 & 2\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 36 & 66\\
  12 & 12 & 22\\
  22 & 22 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 1 & 4\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 6\\
  2 & 6 & 2\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 33 & 66\\
  22 & 16 & 42\\
  22 & 21 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 3 & 6\\
  2 & 2 & 5\\
  2 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 9\\
  4 & 4 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 33 & 69\\
  24 & 24 & 52\\
  22 & 22 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  6 & 1 & 2\\
  4 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 4 & 9\\
  3 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 11 & 31\\
  61 & 14 & 29\\
  43 & 12 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 2 & 7\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 2\\
  7 & 7 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 13 & 32\\
  27 & 27 & 73\\
  21 & 21 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 2 & 7\\
  4 & 3 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 2\\
  7 & 4 & 3\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 11 & 32\\
  27 & 24 & 73\\
  41 & 32 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 2 & 7\\
  2 & 3 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 2\\
  4 & 7 & 3\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 13 & 32\\
  24 & 27 & 73\\
  22 & 31 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 2 & 7\\
  2 & 2 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 4\\
  4 & 4 & 6\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 11 & 34\\
  24 & 24 & 76\\
  22 & 22 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  1 & 1 & 8\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 8\\
  7 & 7 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 12 & 38\\
  17 & 17 & 83\\
  11 & 11 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  4 & 1 & 2\\
  6 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  4 & 1 & 6\\
  2 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 11 & 31\\
  44 & 11 & 26\\
  62 & 13 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 1 & 8\\
  2 & 2 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 4\\
  4 & 7 & 3\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 11 & 34\\
  24 & 17 & 83\\
  22 & 21 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 1 & 8\\
  1 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 8\\
  4 & 7 & 6\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 11 & 38\\
  24 & 17 & 86\\
  12 & 11 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 1 & 8\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  7 & 7 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 34\\
  27 & 17 & 83\\
  21 & 11 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  1 & 1 & 9\\
  1 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 9\\
  7 & 7 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 11 & 39\\
  17 & 17 & 93\\
  11 & 11 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 1 & 3\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 3\\
  4 & 4 & 6\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 12 & 33\\
  24 & 14 & 36\\
  22 & 12 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  3 & 2 & 7\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 4\\
  4 & 7 & 9\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 12 & 34\\
  34 & 27 & 79\\
  12 & 11 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  4 & 1 & 6\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  4 & 7 & 6\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 11 & 33\\
  44 & 17 & 66\\
  22 & 11 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  5 & 1 & 5\\
  5 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  4 & 7 & 3\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 11 & 31\\
  54 & 17 & 53\\
  52 & 21 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  5 & 1 & 5\\
  5 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 1\\
  1 & 7 & 3\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 11 & 31\\
  51 & 17 & 53\\
  53 & 31 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  5 & 2 & 8\\
  5 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 1\\
  7 & 7 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 11 & 31\\
  57 & 27 & 83\\
  51 & 21 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  5 & 2 & 8\\
  5 & 4 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 1 & 1\\
  1 & 7 & 3\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  15 & 11 & 31\\
  51 & 27 & 83\\
  53 & 41 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  5 & 2 & 8\\
  5 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  4 & 7 & 3\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 31\\
  54 & 27 & 83\\
  52 & 31 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  6 & 1 & 4\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  1 & 7 & 9\\
  3 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 11 & 32\\
  61 & 17 & 49\\
  23 & 11 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 1 & 2\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 1\\
  1 & 7 & 3\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 14 & 31\\
  21 & 17 & 23\\
  23 & 31 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 1 & 2\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  4 & 4 & 3\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 13 & 31\\
  24 & 14 & 23\\
  42 & 22 & 19\\
\end{bmatrix}
```
