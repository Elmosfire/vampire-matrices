```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 8\\
  2 & 4 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 4\\
  1 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 21 & 44\\
  21 & 36 & 84\\
  22 & 42 & 88\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 2 & 3\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 1\\
  3 & 6 & 4\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 24 & 41\\
  23 & 26 & 34\\
  21 & 22 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 2 & 3\\
  4 & 3 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 1\\
  3 & 2 & 4\\
  1 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 23 & 41\\
  23 & 22 & 34\\
  41 & 34 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  4 & 2 & 5\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 3\\
  3 & 6 & 8\\
  1 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 21 & 43\\
  43 & 26 & 58\\
  21 & 12 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  6 & 3 & 8\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 2\\
  1 & 8 & 6\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 21 & 42\\
  61 & 38 & 86\\
  22 & 21 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  5 & 2 & 7\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 1\\
  1 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 21 & 41\\
  51 & 28 & 72\\
  52 & 51 & 69\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  5 & 2 & 7\\
  5 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  3 & 8 & 2\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 21 & 41\\
  53 & 28 & 72\\
  51 & 31 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 8 & 2\\
  1 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 28 & 42\\
  11 & 18 & 22\\
  12 & 41 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  3 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  1 & 4 & 2\\
  2 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 24 & 42\\
  11 & 14 & 22\\
  32 & 43 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 2\\
  1 & 6 & 2\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 26 & 42\\
  11 & 16 & 22\\
  22 & 42 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 4\\
  1 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 26 & 44\\
  11 & 16 & 24\\
  12 & 22 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  4 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  1 & 2 & 2\\
  2 & 4 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 22 & 42\\
  11 & 12 & 22\\
  42 & 44 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 4\\
  3 & 2 & 4\\
  1 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 22 & 44\\
  13 & 12 & 24\\
  21 & 14 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  3 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 2\\
  3 & 4 & 2\\
  1 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 24 & 42\\
  13 & 14 & 22\\
  31 & 23 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  3 & 2 & 2\\
  1 & 4 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 22 & 42\\
  13 & 12 & 22\\
  41 & 24 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 2\\
  2 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 4\\
  1 & 2 & 4\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 22 & 44\\
  11 & 12 & 24\\
  22 & 24 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  6 & 3 & 6\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  3 & 6 & 6\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 21 & 41\\
  63 & 36 & 66\\
  41 & 22 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 6\\
  2 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 3\\
  1 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 22 & 43\\
  21 & 18 & 62\\
  22 & 41 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 6\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 3\\
  3 & 8 & 2\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 22 & 43\\
  23 & 18 & 62\\
  21 & 21 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 6\\
  1 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 6\\
  1 & 8 & 4\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 22 & 46\\
  21 & 18 & 64\\
  12 & 21 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  4 & 6 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 1\\
  3 & 6 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 23 & 41\\
  23 & 36 & 62\\
  41 & 62 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  3 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  3 & 4 & 4\\
  1 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 22 & 42\\
  23 & 34 & 64\\
  31 & 43 & 78\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 1\\
  3 & 8 & 2\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 24 & 41\\
  23 & 38 & 62\\
  21 & 41 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  3 & 5 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 2\\
  1 & 4 & 4\\
  2 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 22 & 42\\
  21 & 34 & 64\\
  32 & 53 & 98\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  1 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 2\\
  1 & 8 & 4\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 24 & 42\\
  21 & 38 & 64\\
  12 & 31 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  2 & 4 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 2\\
  1 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 23 & 42\\
  21 & 36 & 64\\
  22 & 42 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 2\\
  3 & 8 & 4\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 24 & 42\\
  23 & 38 & 64\\
  11 & 21 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  2 & 3 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 2\\
  3 & 6 & 4\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 23 & 42\\
  23 & 36 & 64\\
  21 & 32 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 3 & 6\\
  2 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 1\\
  1 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 24 & 41\\
  21 & 38 & 62\\
  22 & 61 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 2 & 6\\
  1 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 8\\
  3 & 6 & 4\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 22 & 48\\
  13 & 26 & 64\\
  11 & 22 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 2 & 6\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 6 & 4\\
  3 & 8 & 2\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 26 & 44\\
  13 & 28 & 62\\
  11 & 31 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 2 & 6\\
  2 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 4\\
  3 & 6 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 22 & 44\\
  13 & 26 & 62\\
  21 & 42 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 3\\
  3 & 4 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 3\\
  1 & 4 & 2\\
  2 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 21 & 43\\
  11 & 14 & 32\\
  32 & 43 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 3\\
  3 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  3 & 4 & 2\\
  1 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 21 & 43\\
  13 & 14 & 32\\
  31 & 23 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 3\\
  1 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 7 & 3\\
  1 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 27 & 43\\
  11 & 18 & 32\\
  12 & 41 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 3\\
  2 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 3\\
  1 & 6 & 2\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 24 & 43\\
  11 & 16 & 32\\
  22 & 42 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 3\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 3\\
  3 & 6 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 24 & 43\\
  13 & 16 & 32\\
  21 & 22 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 1 & 3\\
  1 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 6\\
  1 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 24 & 46\\
  11 & 16 & 34\\
  12 & 22 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  3 & 3 & 8\\
  1 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 4\\
  1 & 8 & 6\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 22 & 44\\
  31 & 38 & 86\\
  12 & 21 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 4\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 2\\
  1 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 23 & 42\\
  21 & 18 & 42\\
  22 & 41 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 4\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 4\\
  1 & 8 & 4\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 23 & 44\\
  21 & 18 & 44\\
  12 & 21 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 4\\
  4 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 2\\
  1 & 6 & 2\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 21 & 42\\
  21 & 16 & 42\\
  42 & 42 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 4\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 4\\
  3 & 6 & 4\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 21 & 44\\
  23 & 16 & 44\\
  21 & 12 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 4\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  3 & 6 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 21 & 42\\
  23 & 16 & 42\\
  41 & 22 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 1 & 4\\
  2 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  1 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 21 & 44\\
  21 & 16 & 44\\
  22 & 22 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 2 & 5\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 3\\
  3 & 6 & 4\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 22 & 43\\
  23 & 26 & 54\\
  21 & 22 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  2 & 2 & 5\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 6\\
  3 & 6 & 8\\
  1 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 22 & 46\\
  23 & 26 & 58\\
  11 & 12 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 2 & 3\\
  4 & 6 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 1\\
  3 & 2 & 2\\
  1 & 4 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 26 & 41\\
  13 & 22 & 32\\
  41 & 64 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 2 & 3\\
  3 & 5 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 7 & 1\\
  3 & 4 & 2\\
  1 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 27 & 41\\
  13 & 24 & 32\\
  31 & 53 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 2 & 3\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 8 & 1\\
  3 & 6 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 28 & 41\\
  13 & 26 & 32\\
  21 & 42 & 19\\
\end{bmatrix}
```
