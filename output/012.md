```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  4 & 2 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 11 & 44\\
  22 & 11 & 44\\
  44 & 22 & 88\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  3 & 1 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  6 & 1 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 11 & 44\\
  26 & 11 & 44\\
  32 & 12 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  5 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  8 & 1 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 11 & 42\\
  28 & 11 & 42\\
  51 & 12 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 4\\
  6 & 3 & 4\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 13 & 44\\
  26 & 13 & 44\\
  22 & 11 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  3 & 2 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 4\\
  2 & 3 & 4\\
  4 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 13 & 44\\
  22 & 13 & 44\\
  34 & 21 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 2\\
  6 & 3 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 13 & 42\\
  26 & 13 & 42\\
  42 & 21 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  6 & 2 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 2\\
  6 & 1 & 2\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 11 & 42\\
  26 & 11 & 42\\
  62 & 22 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 1 & 4\\
  5 & 3 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 2\\
  4 & 3 & 2\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 13 & 42\\
  24 & 13 & 42\\
  53 & 31 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  6 & 2 & 9\\
  7 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 1\\
  9 & 5 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 11 & 41\\
  69 & 25 & 93\\
  71 & 21 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 4\\
  6 & 2 & 9\\
  8 & 3 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  6 & 5 & 3\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 11 & 41\\
  66 & 25 & 93\\
  82 & 31 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  4 & 3 & 6\\
  3 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 5\\
  3 & 4 & 5\\
  1 & 3 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 41 & 75\\
  43 & 34 & 65\\
  31 & 23 & 45\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  4 & 3 & 6\\
  7 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 1\\
  3 & 6 & 1\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 43 & 71\\
  43 & 36 & 61\\
  71 & 61 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  4 & 3 & 6\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 2\\
  2 & 6 & 2\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 43 & 72\\
  42 & 36 & 62\\
  52 & 51 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  4 & 3 & 6\\
  7 & 6 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  2 & 5 & 2\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 42 & 72\\
  42 & 35 & 62\\
  72 & 62 & 98\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  4 & 3 & 6\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 5\\
  2 & 6 & 5\\
  2 & 1 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 43 & 75\\
  42 & 36 & 65\\
  22 & 21 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  3 & 3 & 4\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 2\\
  2 & 2 & 9\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 46 & 72\\
  32 & 32 & 49\\
  22 & 22 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 2 & 3\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 7 & 1\\
  3 & 3 & 1\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 47 & 71\\
  23 & 23 & 31\\
  51 & 51 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 2 & 3\\
  3 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  1 & 1 & 5\\
  3 & 3 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  45 & 45 & 75\\
  21 & 21 & 35\\
  33 & 33 & 55\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 2 & 3\\
  7 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 6 & 1\\
  3 & 2 & 1\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 46 & 71\\
  23 & 22 & 31\\
  71 & 62 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 2 & 3\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 2\\
  2 & 2 & 2\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 46 & 72\\
  22 & 22 & 32\\
  52 & 52 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 2 & 3\\
  9 & 7 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 5 & 1\\
  3 & 1 & 1\\
  1 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 45 & 71\\
  23 & 21 & 31\\
  91 & 73 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 2 & 3\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 5\\
  2 & 2 & 5\\
  2 & 2 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 46 & 75\\
  22 & 22 & 35\\
  22 & 22 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 2 & 3\\
  8 & 9 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 7 & 1\\
  2 & 3 & 1\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 47 & 71\\
  22 & 23 & 31\\
  82 & 91 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  1 & 1 & 2\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 6 & 8\\
  2 & 2 & 1\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 46 & 78\\
  12 & 12 & 21\\
  22 & 22 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  2 & 1 & 4\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 8\\
  2 & 6 & 1\\
  2 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 43 & 78\\
  22 & 16 & 41\\
  22 & 21 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 4 & 7\\
  6 & 5 & 8\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 2\\
  2 & 6 & 9\\
  2 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 43 & 72\\
  62 & 56 & 89\\
  22 & 21 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 3 & 2\\
  5 & 2 & 1\\
  7 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  2 & 2 & 4\\
  8 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 33 & 21\\
  52 & 22 & 14\\
  78 & 33 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 3 & 2\\
  8 & 3 & 2\\
  6 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  6 & 4 & 2\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 32 & 21\\
  86 & 34 & 22\\
  62 & 23 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 3 & 2\\
  8 & 2 & 2\\
  4 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 2\\
  4 & 6 & 2\\
  2 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  79 & 31 & 22\\
  84 & 26 & 22\\
  42 & 13 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 3 & 7\\
  7 & 2 & 4\\
  9 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  3 & 1 & 5\\
  1 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  79 & 31 & 71\\
  73 & 21 & 45\\
  91 & 23 & 47\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 3 & 7\\
  7 & 3 & 7\\
  3 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 7\\
  7 & 1 & 7\\
  1 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  77 & 31 & 77\\
  77 & 31 & 77\\
  31 & 13 & 31\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 9\\
  2 & 2 & 8\\
  3 & 4 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 3 & 3\\
  2 & 6 & 2\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  25 & 33 & 93\\
  22 & 26 & 82\\
  31 & 41 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 9\\
  1 & 1 & 2\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 6 & 6\\
  1 & 2 & 7\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 36 & 96\\
  11 & 12 & 27\\
  11 & 12 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 9\\
  2 & 1 & 4\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 6\\
  1 & 6 & 7\\
  1 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 33 & 96\\
  21 & 16 & 47\\
  11 & 11 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 6 & 7\\
  4 & 5 & 5\\
  6 & 8 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 7 & 1\\
  1 & 3 & 4\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  54 & 67 & 71\\
  41 & 53 & 54\\
  64 & 82 & 86\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 6 & 7\\
  3 & 3 & 4\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 4\\
  2 & 4 & 2\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 62 & 74\\
  32 & 34 & 42\\
  52 & 54 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 6 & 7\\
  3 & 3 & 4\\
  7 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  2 & 5 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 64 & 72\\
  32 & 35 & 41\\
  72 & 82 & 78\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 6 & 7\\
  8 & 8 & 9\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  2 & 4 & 8\\
  2 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 62 & 72\\
  82 & 84 & 98\\
  52 & 54 & 62\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 6 & 7\\
  6 & 5 & 8\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  2 & 7 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 61 & 74\\
  62 & 57 & 82\\
  52 & 52 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 6 & 8\\
  1 & 1 & 2\\
  3 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 6\\
  1 & 4 & 1\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 64 & 86\\
  11 & 14 & 21\\
  32 & 43 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 6 & 8\\
  4 & 5 & 6\\
  3 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  1 & 4 & 7\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 64 & 82\\
  41 & 54 & 67\\
  32 & 43 & 54\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 6 & 8\\
  2 & 3 & 4\\
  4 & 6 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 4\\
  2 & 3 & 2\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 66 & 84\\
  22 & 33 & 42\\
  42 & 63 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 6 & 8\\
  2 & 3 & 4\\
  3 & 4 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 8\\
  2 & 1 & 4\\
  2 & 6 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 62 & 88\\
  22 & 31 & 44\\
  32 & 46 & 64\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 6 & 8\\
  2 & 3 & 4\\
  2 & 3 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 8\\
  2 & 3 & 4\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 66 & 88\\
  22 & 33 & 44\\
  22 & 33 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 6 & 8\\
  3 & 5 & 6\\
  5 & 8 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 8 & 2\\
  3 & 2 & 3\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  42 & 68 & 82\\
  33 & 52 & 63\\
  52 & 83 & 97\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 7 & 3\\
  6 & 5 & 2\\
  7 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 1\\
  4 & 3 & 2\\
  1 & 7 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  87 & 74 & 31\\
  64 & 53 & 22\\
  71 & 57 & 23\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 7 & 3\\
  3 & 2 & 1\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 2\\
  2 & 4 & 1\\
  1 & 7 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  89 & 73 & 32\\
  32 & 24 & 11\\
  41 & 27 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 7 & 3\\
  8 & 4 & 2\\
  9 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  2 & 6 & 2\\
  1 & 7 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  89 & 71 & 31\\
  82 & 46 & 22\\
  91 & 47 & 23\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 3 & 8\\
  9 & 2 & 6\\
  8 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  3 & 5 & 3\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  89 & 31 & 81\\
  93 & 25 & 63\\
  81 & 21 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 3 & 8\\
  9 & 2 & 6\\
  4 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 2\\
  3 & 5 & 6\\
  1 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  89 & 31 & 82\\
  93 & 25 & 66\\
  41 & 11 & 26\\
\end{bmatrix}
```
