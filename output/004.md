```math
\begin{bmatrix}
  2 & 3 & 2\\
  5 & 6 & 4\\
  6 & 8 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 1\\
  2 & 6 & 3\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 32 & 21\\
  52 & 66 & 43\\
  65 & 85 & 55\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 6 & 9\\
  1 & 2 & 4\\
  2 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 9 & 3\\
  1 & 7 & 1\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  25 & 69 & 93\\
  11 & 27 & 41\\
  21 & 71 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 6 & 9\\
  1 & 2 & 4\\
  3 & 8 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 6 & 3\\
  1 & 6 & 1\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  25 & 66 & 93\\
  11 & 26 & 41\\
  31 & 82 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 6 & 9\\
  2 & 3 & 8\\
  3 & 8 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 3 & 3\\
  1 & 8 & 1\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  25 & 63 & 93\\
  21 & 38 & 81\\
  31 & 81 & 89\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 9 & 3\\
  3 & 8 & 4\\
  2 & 8 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 3 & 3\\
  1 & 9 & 1\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  25 & 93 & 33\\
  31 & 89 & 41\\
  22 & 82 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 9 & 3\\
  1 & 2 & 1\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 6 & 3\\
  1 & 7 & 1\\
  1 & 7 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 96 & 33\\
  11 & 27 & 11\\
  21 & 47 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 9 & 3\\
  1 & 2 & 1\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 6 & 6\\
  1 & 7 & 2\\
  1 & 7 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 96 & 36\\
  11 & 27 & 12\\
  11 & 27 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 4\\
  7 & 4 & 3\\
  9 & 5 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  3 & 4 & 2\\
  4 & 7 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  97 & 61 & 43\\
  73 & 44 & 32\\
  94 & 57 & 41\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 4\\
  4 & 2 & 1\\
  6 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 1\\
  1 & 2 & 4\\
  3 & 6 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  99 & 63 & 41\\
  41 & 22 & 14\\
  63 & 36 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 4\\
  5 & 3 & 2\\
  8 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  3 & 2 & 2\\
  2 & 8 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  98 & 62 & 42\\
  53 & 32 & 22\\
  82 & 48 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 4\\
  5 & 3 & 2\\
  3 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 2\\
  3 & 4 & 2\\
  2 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  98 & 64 & 42\\
  53 & 34 & 22\\
  32 & 21 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 4\\
  3 & 1 & 1\\
  5 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 3\\
  1 & 4 & 2\\
  3 & 7 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  99 & 61 & 43\\
  31 & 14 & 12\\
  53 & 27 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 4\\
  9 & 6 & 4\\
  7 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 4 & 2\\
  5 & 4 & 2\\
  5 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  95 & 64 & 42\\
  95 & 64 & 42\\
  75 & 51 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  2 & 2 & 7\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 1\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 14 & 51\\
  25 & 25 & 75\\
  21 & 21 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  1 & 1 & 8\\
  1 & 1 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 8\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 12 & 58\\
  15 & 15 & 85\\
  11 & 11 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  2 & 1 & 8\\
  2 & 1 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 54\\
  25 & 15 & 85\\
  21 & 11 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  1 & 1 & 9\\
  1 & 1 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 9\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 11 & 59\\
  15 & 15 & 95\\
  11 & 11 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  1 & 1 & 6\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 6\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 14 & 56\\
  15 & 15 & 65\\
  11 & 11 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  5 & 1 & 5\\
  5 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 11 & 51\\
  55 & 15 & 55\\
  51 & 11 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  2 & 1 & 6\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 3\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 12 & 53\\
  25 & 15 & 65\\
  21 & 11 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  2 & 2 & 9\\
  2 & 2 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 2\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 13 & 52\\
  25 & 25 & 95\\
  21 & 21 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 5\\
  1 & 1 & 7\\
  1 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 7\\
  5 & 5 & 5\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 13 & 57\\
  15 & 15 & 75\\
  11 & 11 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 2 & 7\\
  5 & 1 & 5\\
  5 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 2\\
  4 & 4 & 2\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  69 & 21 & 72\\
  54 & 14 & 52\\
  51 & 11 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 7 & 6\\
  3 & 2 & 2\\
  7 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 4\\
  1 & 3 & 2\\
  4 & 7 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  87 & 71 & 64\\
  31 & 23 & 22\\
  74 & 57 & 53\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 2\\
  9 & 6 & 2\\
  6 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 1\\
  6 & 4 & 1\\
  3 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  96 & 64 & 21\\
  96 & 64 & 21\\
  63 & 42 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 6 & 2\\
  5 & 3 & 1\\
  6 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 1\\
  3 & 4 & 1\\
  4 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  98 & 64 & 21\\
  53 & 34 & 11\\
  64 & 42 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 2 & 8\\
  8 & 2 & 9\\
  4 & 1 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 5\\
  6 & 3 & 5\\
  2 & 1 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  77 & 21 & 85\\
  86 & 23 & 95\\
  42 & 11 & 45\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 5 & 3\\
  2 & 6 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 7 & 1\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 88 & 44\\
  22 & 57 & 31\\
  22 & 62 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 5 & 3\\
  2 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 8\\
  2 & 6 & 2\\
  2 & 6 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 84 & 48\\
  22 & 56 & 32\\
  22 & 56 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 6 & 3\\
  3 & 8 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 8 & 4\\
  3 & 4 & 2\\
  1 & 8 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 88 & 44\\
  23 & 64 & 32\\
  31 & 88 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  4 & 9 & 5\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 4\\
  1 & 8 & 3\\
  4 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 84 & 44\\
  41 & 98 & 53\\
  24 & 62 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 1 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 4\\
  1 & 8 & 1\\
  1 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 82 & 44\\
  21 & 18 & 21\\
  21 & 23 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  4 & 3 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 2\\
  1 & 8 & 3\\
  1 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 82 & 42\\
  41 & 38 & 23\\
  21 & 23 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 3 & 2\\
  3 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 4\\
  1 & 6 & 2\\
  2 & 7 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 82 & 44\\
  21 & 36 & 22\\
  32 & 57 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 3 & 2\\
  2 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 4\\
  1 & 7 & 2\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 84 & 44\\
  21 & 37 & 22\\
  22 & 44 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 3 & 2\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 6 & 2\\
  1 & 8 & 1\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 86 & 42\\
  21 & 38 & 21\\
  22 & 61 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 3 & 2\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 6 & 4\\
  1 & 8 & 2\\
  2 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 86 & 44\\
  21 & 38 & 22\\
  12 & 31 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 3 & 2\\
  4 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 2\\
  1 & 7 & 1\\
  2 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 84 & 42\\
  21 & 37 & 21\\
  42 & 84 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  4 & 5 & 6\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 8\\
  1 & 9 & 1\\
  4 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 82 & 48\\
  41 & 59 & 61\\
  24 & 61 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 3 & 3\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 8\\
  1 & 8 & 1\\
  4 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 84 & 48\\
  21 & 38 & 31\\
  24 & 62 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  3 & 6 & 4\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 6\\
  1 & 8 & 2\\
  4 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 84 & 46\\
  31 & 68 & 42\\
  24 & 62 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  3 & 7 & 5\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 8\\
  2 & 8 & 1\\
  3 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 84 & 48\\
  32 & 78 & 51\\
  23 & 62 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  2 & 2 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 4 & 2\\
  1 & 6 & 3\\
  1 & 6 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 84 & 42\\
  21 & 26 & 13\\
  21 & 26 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  4 & 9 & 6\\
  2 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 8\\
  2 & 8 & 2\\
  2 & 3 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 82 & 48\\
  42 & 98 & 62\\
  22 & 53 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  5 & 6 & 5\\
  3 & 7 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  1 & 9 & 1\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 82 & 42\\
  51 & 69 & 51\\
  32 & 71 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  1 & 2 & 1\\
  2 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 8 & 4\\
  1 & 4 & 2\\
  2 & 8 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 88 & 44\\
  11 & 24 & 12\\
  22 & 48 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 4\\
  1 & 2 & 1\\
  4 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 8 & 2\\
  1 & 4 & 1\\
  2 & 8 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 88 & 42\\
  11 & 24 & 11\\
  42 & 88 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 6\\
  2 & 2 & 7\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 8\\
  5 & 5 & 5\\
  1 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 24 & 68\\
  25 & 25 & 75\\
  11 & 11 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 6\\
  3 & 2 & 8\\
  4 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  4 & 6 & 2\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 22 & 62\\
  34 & 26 & 82\\
  41 & 31 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 6\\
  1 & 1 & 4\\
  3 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 6 & 4\\
  3 & 4 & 1\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 26 & 64\\
  13 & 14 & 41\\
  32 & 41 & 79\\
\end{bmatrix}
```
