```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 4 & 1\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 1\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 84 & 21\\
  21 & 48 & 12\\
  21 & 48 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 9 & 4\\
  1 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 8\\
  1 & 9 & 2\\
  1 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 81 & 28\\
  21 & 99 & 42\\
  11 & 54 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 5 & 2\\
  1 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 4\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 86 & 24\\
  11 & 59 & 21\\
  11 & 64 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 5 & 2\\
  1 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 8\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 82 & 28\\
  11 & 58 & 22\\
  11 & 58 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 4 & 1\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 8 & 2\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 88 & 22\\
  11 & 48 & 12\\
  11 & 48 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 4 & 1\\
  2 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 8 & 1\\
  1 & 8 & 1\\
  1 & 8 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 88 & 21\\
  11 & 48 & 11\\
  21 & 88 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 3 & 1\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 6 & 2\\
  1 & 8 & 1\\
  1 & 8 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 86 & 22\\
  11 & 38 & 11\\
  21 & 68 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 3 & 1\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 6 & 4\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 86 & 24\\
  11 & 38 & 12\\
  11 & 38 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 5 & 2\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 2\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 83 & 22\\
  21 & 59 & 21\\
  21 & 64 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 5 & 2\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 4\\
  1 & 9 & 2\\
  1 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 83 & 24\\
  21 & 59 & 22\\
  11 & 34 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 5 & 2\\
  2 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 4\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 81 & 24\\
  21 & 58 & 22\\
  21 & 58 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 1 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 4\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 81 & 24\\
  21 & 19 & 21\\
  21 & 24 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 2 & 2\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 7\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 83 & 27\\
  11 & 29 & 21\\
  11 & 34 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  4 & 3 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 3\\
  1 & 9 & 2\\
  1 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 81 & 23\\
  41 & 39 & 22\\
  21 & 24 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 4 & 2\\
  1 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  15 & 85 & 25\\
  11 & 49 & 21\\
  11 & 54 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 3 & 2\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 3\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 82 & 23\\
  21 & 39 & 21\\
  21 & 44 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 3 & 2\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 6\\
  1 & 9 & 2\\
  1 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 82 & 26\\
  21 & 39 & 22\\
  11 & 24 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 5 & 3\\
  1 & 6 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 8\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 82 & 28\\
  11 & 59 & 31\\
  11 & 64 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 1 & 2\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 8\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 82 & 28\\
  11 & 19 & 21\\
  11 & 24 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 7 & 2\\
  1 & 7 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 6 & 4\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 86 & 24\\
  11 & 78 & 22\\
  11 & 78 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 7 & 2\\
  1 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 8 & 2\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 88 & 22\\
  11 & 79 & 21\\
  11 & 84 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 3 & 2\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 6\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 84 & 26\\
  11 & 39 & 21\\
  11 & 44 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  5 & 4 & 2\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 81 & 21\\
  51 & 49 & 21\\
  51 & 54 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  4 & 7 & 2\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  1 & 9 & 2\\
  1 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 82 & 21\\
  41 & 79 & 22\\
  21 & 44 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 2 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 3\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 82 & 23\\
  21 & 28 & 12\\
  21 & 28 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 4 & 3\\
  1 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 9\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 81 & 29\\
  11 & 49 & 31\\
  11 & 54 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 7 & 2\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  1 & 9 & 2\\
  1 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 84 & 22\\
  21 & 79 & 22\\
  11 & 44 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 7 & 2\\
  2 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 1\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 84 & 21\\
  21 & 79 & 21\\
  21 & 84 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 7 & 2\\
  2 & 7 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 2\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 83 & 22\\
  21 & 78 & 22\\
  21 & 78 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 2 & 1\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 3\\
  1 & 8 & 1\\
  1 & 8 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 84 & 23\\
  11 & 28 & 11\\
  21 & 48 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 2\\
  1 & 2 & 1\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 6\\
  1 & 8 & 2\\
  1 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 84 & 26\\
  11 & 28 & 12\\
  11 & 28 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 9 & 8\\
  1 & 4 & 4\\
  2 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 5 & 6\\
  1 & 5 & 2\\
  1 & 5 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 95 & 86\\
  11 & 45 & 42\\
  21 & 85 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 4 & 7\\
  8 & 3 & 5\\
  6 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 3\\
  3 & 1 & 8\\
  2 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  98 & 41 & 73\\
  83 & 31 & 58\\
  62 & 24 & 42\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 7\\
  2 & 1 & 8\\
  3 & 1 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  7 & 2 & 3\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 11 & 74\\
  27 & 12 & 83\\
  31 & 11 & 69\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 7\\
  2 & 1 & 8\\
  2 & 1 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 8\\
  4 & 2 & 6\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 11 & 78\\
  24 & 12 & 86\\
  22 & 11 & 78\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 7\\
  5 & 1 & 5\\
  6 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  4 & 2 & 6\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 11 & 71\\
  54 & 12 & 56\\
  61 & 11 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 7\\
  2 & 1 & 6\\
  2 & 1 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 6\\
  2 & 1 & 8\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 12 & 76\\
  22 & 11 & 68\\
  22 & 11 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 1 & 7\\
  2 & 1 & 6\\
  3 & 1 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 3\\
  6 & 1 & 4\\
  1 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 12 & 73\\
  26 & 11 & 64\\
  31 & 11 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 5 & 3\\
  4 & 2 & 1\\
  6 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 1\\
  3 & 1 & 2\\
  1 & 7 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  99 & 53 & 31\\
  43 & 21 & 12\\
  61 & 27 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 5 & 3\\
  6 & 3 & 2\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 2\\
  4 & 4 & 1\\
  2 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  98 & 53 & 32\\
  64 & 34 & 21\\
  42 & 22 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  2 & 1 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 4\\
  1 & 8 & 1\\
  2 & 6 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 82 & 24\\
  21 & 18 & 11\\
  22 & 26 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  4 & 8 & 2\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 1\\
  2 & 8 & 2\\
  1 & 4 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 84 & 21\\
  42 & 88 & 22\\
  21 & 44 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  6 & 7 & 2\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  1 & 9 & 2\\
  3 & 2 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 82 & 21\\
  61 & 79 & 22\\
  23 & 42 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  4 & 1 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 4\\
  1 & 9 & 1\\
  2 & 3 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 81 & 24\\
  41 & 19 & 21\\
  22 & 23 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  4 & 5 & 2\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 2\\
  1 & 9 & 1\\
  4 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 83 & 22\\
  41 & 59 & 21\\
  24 & 61 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  2 & 3 & 1\\
  4 & 8 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 2\\
  1 & 7 & 1\\
  4 & 8 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 84 & 22\\
  21 & 37 & 11\\
  44 & 88 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  2 & 3 & 1\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 6 & 2\\
  1 & 8 & 1\\
  4 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 86 & 22\\
  21 & 38 & 11\\
  24 & 62 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  7 & 4 & 2\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 9 & 1\\
  2 & 3 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 81 & 21\\
  71 & 49 & 21\\
  52 & 53 & 15\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  4 & 3 & 2\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 3\\
  1 & 9 & 1\\
  3 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 82 & 23\\
  41 & 39 & 21\\
  23 & 42 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 8 & 2\\
  3 & 7 & 2\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 2\\
  2 & 8 & 1\\
  3 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 86 & 22\\
  32 & 78 & 21\\
  23 & 62 & 14\\
\end{bmatrix}
```
