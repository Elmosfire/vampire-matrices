```math
\begin{bmatrix}
  2 & 2 & 2\\
  5 & 5 & 6\\
  4 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  7 & 7 & 2\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 22 & 22\\
  57 & 57 & 62\\
  42 & 42 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  5 & 5 & 8\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 4\\
  8 & 8 & 1\\
  1 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 22 & 24\\
  58 & 58 & 81\\
  21 & 21 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  4 & 4 & 4\\
  5 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  4 & 4 & 4\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 22 & 22\\
  44 & 44 & 44\\
  55 & 55 & 55\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  6 & 5 & 6\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  6 & 8 & 3\\
  2 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 22 & 22\\
  66 & 58 & 63\\
  22 & 21 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  6 & 5 & 6\\
  3 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  3 & 8 & 3\\
  6 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 22 & 22\\
  63 & 58 & 63\\
  36 & 41 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  6 & 5 & 5\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  4 & 7 & 7\\
  4 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 22 & 22\\
  64 & 57 & 57\\
  24 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  4 & 5 & 5\\
  4 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 4\\
  4 & 7 & 1\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 43 & 34\\
  44 & 57 & 51\\
  44 & 62 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  3 & 3 & 3\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 6\\
  3 & 6 & 3\\
  2 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 42 & 36\\
  33 & 36 & 33\\
  22 & 24 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  4 & 6 & 4\\
  4 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 6 & 1\\
  4 & 4 & 4\\
  4 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 46 & 31\\
  44 & 64 & 44\\
  44 & 64 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  8 & 7 & 7\\
  6 & 7 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 1 & 2\\
  2 & 8 & 2\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  35 & 41 & 32\\
  82 & 78 & 72\\
  64 & 72 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  3 & 2 & 2\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  2 & 7 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 43 & 31\\
  32 & 27 & 21\\
  52 & 52 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  3 & 2 & 2\\
  8 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  2 & 6 & 1\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 42 & 31\\
  32 & 26 & 21\\
  82 & 64 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  3 & 2 & 2\\
  7 & 9 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 1\\
  1 & 7 & 1\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 43 & 31\\
  31 & 27 & 21\\
  74 & 92 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  3 & 2 & 2\\
  4 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  2 & 6 & 2\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 42 & 32\\
  32 & 26 & 22\\
  42 & 34 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 1 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 4\\
  2 & 7 & 1\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 43 & 34\\
  22 & 17 & 21\\
  22 & 22 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 1 & 2\\
  4 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 4\\
  1 & 7 & 1\\
  6 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 43 & 34\\
  21 & 17 & 21\\
  46 & 62 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  4 & 1 & 3\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 6\\
  1 & 8 & 3\\
  4 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 41 & 36\\
  41 & 18 & 33\\
  24 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  6 & 7 & 5\\
  4 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 1\\
  1 & 7 & 4\\
  6 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 43 & 31\\
  61 & 77 & 54\\
  46 & 62 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  4 & 3 & 5\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 7\\
  3 & 8 & 1\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 41 & 37\\
  43 & 38 & 51\\
  22 & 22 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  6 & 3 & 4\\
  8 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  2 & 8 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 41 & 31\\
  62 & 38 & 41\\
  82 & 62 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  6 & 3 & 4\\
  4 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  2 & 8 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 41 & 32\\
  62 & 38 & 42\\
  42 & 32 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  4 & 3 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  2 & 7 & 4\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 43 & 31\\
  42 & 37 & 24\\
  22 & 22 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  4 & 3 & 2\\
  6 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  2 & 3 & 4\\
  2 & 8 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 42 & 31\\
  42 & 33 & 24\\
  62 & 48 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  4 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 7 & 2\\
  4 & 2 & 2\\
  2 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 47 & 32\\
  24 & 32 & 22\\
  42 & 56 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  4 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 9 & 1\\
  4 & 4 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 49 & 31\\
  24 & 34 & 21\\
  42 & 62 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 8 & 2\\
  4 & 3 & 2\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 48 & 32\\
  24 & 33 & 22\\
  32 & 44 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  5 & 8 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 8 & 2\\
  2 & 3 & 2\\
  6 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  32 & 48 & 32\\
  22 & 33 & 22\\
  56 & 84 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  5 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 2\\
  4 & 1 & 2\\
  2 & 8 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 46 & 32\\
  24 & 31 & 22\\
  52 & 68 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  2 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 9 & 2\\
  4 & 4 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 49 & 32\\
  24 & 34 & 22\\
  22 & 32 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  6 & 9 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 7 & 2\\
  2 & 2 & 2\\
  6 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  32 & 47 & 32\\
  22 & 32 & 22\\
  66 & 96 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  6 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 8 & 1\\
  4 & 3 & 1\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 48 & 31\\
  24 & 33 & 21\\
  62 & 84 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 3 & 2\\
  4 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 9 & 2\\
  2 & 4 & 2\\
  6 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  32 & 49 & 32\\
  22 & 34 & 22\\
  46 & 72 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 2 & 2\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  2 & 6 & 2\\
  4 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  35 & 45 & 35\\
  22 & 26 & 22\\
  24 & 32 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  2 & 2 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 6 & 1\\
  2 & 4 & 4\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 46 & 31\\
  22 & 24 & 14\\
  22 & 24 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  6 & 5 & 6\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 6\\
  3 & 8 & 3\\
  2 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 41 & 36\\
  63 & 58 & 63\\
  22 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  5 & 6 & 5\\
  6 & 7 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  4 & 6 & 2\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 42 & 32\\
  54 & 66 & 52\\
  62 & 74 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  5 & 6 & 5\\
  7 & 9 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 1\\
  4 & 7 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 43 & 31\\
  54 & 67 & 51\\
  72 & 92 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 3\\
  5 & 7 & 5\\
  3 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 2\\
  5 & 5 & 5\\
  2 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  32 & 44 & 32\\
  55 & 75 & 55\\
  32 & 44 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  3 & 2 & 3\\
  6 & 4 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 3\\
  3 & 2 & 3\\
  6 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 22 & 33\\
  33 & 22 & 33\\
  66 & 44 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  3 & 2 & 3\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 6\\
  6 & 2 & 6\\
  2 & 4 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 22 & 36\\
  36 & 22 & 36\\
  22 & 14 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  3 & 2 & 3\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 3\\
  6 & 2 & 3\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 22 & 33\\
  36 & 22 & 33\\
  42 & 24 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  2 & 1 & 1\\
  8 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  1 & 1 & 2\\
  4 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 23 & 31\\
  21 & 11 & 12\\
  84 & 44 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  2 & 1 & 1\\
  3 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 2\\
  1 & 3 & 4\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 24 & 32\\
  21 & 13 & 14\\
  34 & 22 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  2 & 1 & 1\\
  3 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 2\\
  3 & 1 & 4\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 23 & 32\\
  23 & 11 & 14\\
  32 & 14 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  2 & 1 & 1\\
  6 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 1\\
  3 & 1 & 2\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 23 & 31\\
  23 & 11 & 12\\
  62 & 24 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  2 & 1 & 1\\
  6 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 1\\
  1 & 3 & 2\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 24 & 31\\
  21 & 13 & 12\\
  64 & 42 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  4 & 2 & 2\\
  2 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  4 & 4 & 8\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 23 & 31\\
  44 & 24 & 28\\
  22 & 12 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  8 & 5 & 7\\
  6 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 1\\
  5 & 5 & 5\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 22 & 31\\
  85 & 55 & 75\\
  64 & 42 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  6 & 2 & 4\\
  8 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  4 & 6 & 2\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  38 & 21 & 31\\
  64 & 26 & 42\\
  82 & 32 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 3\\
  5 & 3 & 4\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  6 & 2 & 7\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 22 & 32\\
  56 & 32 & 47\\
  42 & 24 & 34\\
\end{bmatrix}
```
