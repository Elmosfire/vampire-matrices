```math
\begin{bmatrix}
  8 & 9 & 8\\
  3 & 2 & 2\\
  4 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 2\\
  1 & 5 & 2\\
  1 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  89 & 93 & 82\\
  31 & 25 & 22\\
  41 & 33 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 9 & 8\\
  5 & 6 & 5\\
  6 & 7 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 8 & 2\\
  4 & 2 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  84 & 98 & 82\\
  54 & 62 & 52\\
  62 & 72 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 2 & 5\\
  8 & 3 & 8\\
  3 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 6\\
  8 & 3 & 8\\
  2 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 21 & 56\\
  88 & 33 & 88\\
  32 & 12 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 2 & 5\\
  8 & 3 & 8\\
  6 & 2 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 3\\
  8 & 3 & 4\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 21 & 53\\
  88 & 33 & 84\\
  62 & 22 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 2 & 5\\
  8 & 3 & 6\\
  6 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 1\\
  6 & 1 & 8\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  57 & 22 & 51\\
  86 & 31 & 68\\
  62 & 22 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 2 & 5\\
  4 & 1 & 3\\
  3 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 6\\
  1 & 3 & 8\\
  4 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  57 & 21 & 56\\
  41 & 13 & 38\\
  34 & 12 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 2 & 5\\
  4 & 1 & 3\\
  6 & 2 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  1 & 3 & 4\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  57 & 21 & 53\\
  41 & 13 & 34\\
  64 & 22 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 2 & 5\\
  5 & 2 & 6\\
  3 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 7\\
  8 & 3 & 1\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 21 & 57\\
  58 & 23 & 61\\
  32 & 12 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 2 & 5\\
  7 & 3 & 7\\
  5 & 2 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 4\\
  7 & 1 & 7\\
  4 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  54 & 22 & 54\\
  77 & 31 & 77\\
  54 & 22 & 54\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 2 & 6\\
  6 & 1 & 4\\
  5 & 1 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 4\\
  4 & 3 & 2\\
  3 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  98 & 21 & 64\\
  64 & 13 & 42\\
  53 & 11 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 2 & 6\\
  6 & 1 & 4\\
  9 & 2 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  3 & 3 & 2\\
  6 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  96 & 21 & 64\\
  63 & 13 & 42\\
  96 & 21 & 64\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 7 & 6\\
  3 & 2 & 8\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 5\\
  1 & 9 & 1\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 71 & 65\\
  31 & 29 & 81\\
  21 & 41 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 7 & 6\\
  2 & 7 & 6\\
  1 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 8 & 3\\
  1 & 8 & 3\\
  2 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  21 & 78 & 63\\
  21 & 78 & 63\\
  12 & 51 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 7 & 6\\
  4 & 4 & 9\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 9\\
  1 & 9 & 3\\
  1 & 1 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 71 & 69\\
  41 & 49 & 93\\
  11 & 21 & 25\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 7 & 6\\
  1 & 2 & 2\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 5 & 4\\
  1 & 5 & 2\\
  1 & 5 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 75 & 64\\
  11 & 25 & 22\\
  21 & 45 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 7 & 6\\
  1 & 2 & 2\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 5 & 8\\
  1 & 5 & 4\\
  1 & 5 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 75 & 68\\
  11 & 25 & 24\\
  11 & 25 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 7 & 6\\
  1 & 2 & 2\\
  3 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 4\\
  1 & 3 & 2\\
  1 & 8 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 71 & 64\\
  11 & 23 & 22\\
  31 & 58 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 7 & 6\\
  1 & 2 & 2\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 9 & 4\\
  1 & 7 & 2\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 79 & 64\\
  11 & 27 & 22\\
  11 & 32 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 6 & 4\\
  5 & 3 & 2\\
  7 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  2 & 3 & 3\\
  3 & 7 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  88 & 62 & 42\\
  52 & 33 & 23\\
  73 & 47 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 5 & 6\\
  4 & 3 & 4\\
  9 & 7 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 4\\
  2 & 3 & 2\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  64 & 51 & 64\\
  42 & 33 & 42\\
  95 & 75 & 95\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  9 & 7 & 6\\
  7 & 4 & 4\\
  6 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  2 & 6 & 2\\
  2 & 2 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  98 & 72 & 62\\
  72 & 46 & 42\\
  62 & 42 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  7 & 2 & 1\\
  6 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  3 & 7 & 2\\
  4 & 6 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  49 & 31 & 11\\
  73 & 27 & 12\\
  64 & 26 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  4 & 2 & 1\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  4 & 6 & 1\\
  4 & 6 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 32 & 12\\
  44 & 26 & 11\\
  44 & 26 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  5 & 2 & 1\\
  8 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  4 & 6 & 1\\
  1 & 9 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  49 & 31 & 11\\
  54 & 26 & 11\\
  81 & 29 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  5 & 2 & 1\\
  6 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  3 & 7 & 1\\
  7 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 32 & 11\\
  53 & 27 & 11\\
  67 & 43 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  8 & 3 & 2\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  4 & 8 & 1\\
  4 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 31 & 12\\
  84 & 38 & 21\\
  44 & 23 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  4 & 3 & 1\\
  6 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 1\\
  6 & 4 & 1\\
  4 & 6 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 34 & 11\\
  46 & 34 & 11\\
  64 & 46 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  8 & 6 & 2\\
  4 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 1\\
  8 & 6 & 2\\
  4 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 33 & 11\\
  88 & 66 & 22\\
  44 & 33 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  7 & 5 & 2\\
  7 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  7 & 6 & 1\\
  7 & 6 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 32 & 12\\
  77 & 56 & 21\\
  77 & 56 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  8 & 5 & 2\\
  6 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 1\\
  6 & 7 & 1\\
  4 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 32 & 11\\
  86 & 57 & 21\\
  64 & 43 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 1\\
  5 & 3 & 1\\
  4 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 1\\
  4 & 6 & 2\\
  7 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 33 & 11\\
  54 & 36 & 12\\
  47 & 33 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  3 & 2 & 3\\
  5 & 3 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 2\\
  3 & 2 & 3\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 33 & 52\\
  33 & 22 & 33\\
  52 & 33 & 47\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  4 & 3 & 6\\
  3 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 8\\
  6 & 4 & 2\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 31 & 58\\
  46 & 34 & 62\\
  32 & 23 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  4 & 3 & 6\\
  6 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 4\\
  6 & 4 & 1\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 31 & 54\\
  46 & 34 & 61\\
  62 & 43 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  4 & 1 & 2\\
  3 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 3\\
  1 & 4 & 7\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  49 & 31 & 53\\
  41 & 14 & 27\\
  32 & 13 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  7 & 5 & 9\\
  3 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 7\\
  6 & 4 & 8\\
  2 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 31 & 57\\
  76 & 54 & 98\\
  32 & 23 & 41\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  5 & 4 & 7\\
  7 & 5 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 3\\
  7 & 3 & 2\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  43 & 32 & 53\\
  57 & 43 & 72\\
  72 & 53 & 87\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  2 & 1 & 2\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 6\\
  2 & 3 & 4\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 32 & 56\\
  22 & 13 & 24\\
  22 & 13 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  2 & 1 & 2\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 3\\
  2 & 3 & 2\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 32 & 53\\
  22 & 13 & 22\\
  42 & 23 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  4 & 3 & 4\\
  3 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  4 & 1 & 8\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 34 & 52\\
  44 & 31 & 48\\
  32 & 23 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  4 & 3 & 4\\
  6 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 1\\
  4 & 1 & 4\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 34 & 51\\
  44 & 31 & 44\\
  62 & 43 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  6 & 3 & 4\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  2 & 3 & 9\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 32 & 51\\
  62 & 33 & 49\\
  42 & 23 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  7 & 3 & 5\\
  9 & 4 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  2 & 4 & 4\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 31 & 51\\
  72 & 34 & 54\\
  92 & 43 & 67\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  6 & 5 & 8\\
  8 & 6 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 2\\
  8 & 2 & 3\\
  2 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  42 & 33 & 52\\
  68 & 52 & 83\\
  82 & 63 & 97\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 5\\
  6 & 5 & 8\\
  4 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 4\\
  8 & 2 & 6\\
  2 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  42 & 33 & 54\\
  68 & 52 & 86\\
  42 & 33 & 54\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  4 & 3 & 6\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 4\\
  6 & 8 & 1\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 22 & 24\\
  46 & 38 & 61\\
  22 & 21 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  2 & 1 & 1\\
  4 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  2 & 3 & 3\\
  4 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 22 & 22\\
  22 & 13 & 13\\
  44 & 26 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  2 & 1 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 2\\
  2 & 6 & 3\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 24 & 22\\
  22 & 16 & 13\\
  24 & 22 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  5 & 3 & 3\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  1 & 7 & 7\\
  6 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 22 & 22\\
  51 & 37 & 37\\
  26 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 2 & 2\\
  4 & 2 & 2\\
  2 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  4 & 6 & 6\\
  2 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 22 & 22\\
  44 & 26 & 26\\
  22 & 13 & 13\\
\end{bmatrix}
```
