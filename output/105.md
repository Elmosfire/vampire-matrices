```math
\begin{bmatrix}
  7 & 2 & 2\\
  4 & 1 & 1\\
  4 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 2\\
  4 & 2 & 2\\
  4 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  79 & 22 & 22\\
  44 & 12 & 12\\
  44 & 12 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 2 & 2\\
  4 & 1 & 1\\
  8 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 1\\
  4 & 2 & 1\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  79 & 22 & 21\\
  44 & 12 & 11\\
  84 & 22 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 2 & 2\\
  4 & 1 & 1\\
  7 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  3 & 2 & 2\\
  8 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 22 & 22\\
  43 & 12 & 12\\
  78 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 2 & 2\\
  9 & 2 & 2\\
  7 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  7 & 3 & 3\\
  1 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  79 & 21 & 21\\
  97 & 23 & 23\\
  71 & 14 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 2 & 2\\
  7 & 2 & 2\\
  4 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  8 & 2 & 2\\
  3 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 22 & 22\\
  78 & 22 & 22\\
  43 & 12 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 2 & 2\\
  7 & 2 & 2\\
  8 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  8 & 2 & 1\\
  3 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 22 & 21\\
  78 & 22 & 21\\
  83 & 22 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 2 & 2\\
  7 & 2 & 2\\
  7 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  7 & 2 & 2\\
  7 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  77 & 22 & 22\\
  77 & 22 & 22\\
  77 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 9\\
  3 & 3 & 4\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 2\\
  1 & 7 & 3\\
  1 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 74 & 92\\
  31 & 37 & 43\\
  21 & 31 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 9\\
  3 & 3 & 4\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  1 & 4 & 3\\
  1 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 72 & 92\\
  31 & 34 & 43\\
  51 & 54 & 67\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 9\\
  2 & 3 & 4\\
  3 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  1 & 4 & 3\\
  2 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  45 & 75 & 95\\
  21 & 34 & 43\\
  32 & 53 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 9\\
  6 & 5 & 8\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  1 & 7 & 3\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 71 & 92\\
  61 & 57 & 83\\
  51 & 52 & 67\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 7 & 3\\
  4 & 5 & 2\\
  6 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 5 & 1\\
  2 & 5 & 2\\
  4 & 5 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 75 & 31\\
  42 & 55 & 22\\
  64 & 85 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 7 & 3\\
  7 & 2 & 4\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 5\\
  1 & 9 & 1\\
  5 & 1 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  57 & 71 & 35\\
  71 & 29 & 41\\
  35 & 41 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 7 & 3\\
  3 & 4 & 2\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  3 & 5 & 1\\
  3 & 5 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  55 & 75 & 35\\
  33 & 45 & 21\\
  33 & 45 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 7 & 3\\
  2 & 2 & 1\\
  6 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 5 & 2\\
  1 & 5 & 1\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  57 & 75 & 32\\
  21 & 25 & 11\\
  65 & 85 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  5 & 7 & 3\\
  7 & 7 & 4\\
  6 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  2 & 8 & 1\\
  4 & 2 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  56 & 72 & 32\\
  72 & 78 & 41\\
  64 & 82 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  5 & 1 & 5\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 7\\
  1 & 9 & 1\\
  3 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 71 & 47\\
  51 & 19 & 51\\
  23 & 31 & 23\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  3 & 5 & 3\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  3 & 5 & 3\\
  1 & 5 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  45 & 75 & 45\\
  33 & 55 & 33\\
  21 & 35 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  7 & 4 & 6\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 6\\
  1 & 9 & 2\\
  3 & 1 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 71 & 46\\
  71 & 49 & 62\\
  23 & 31 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  7 & 4 & 6\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  1 & 9 & 1\\
  3 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 71 & 43\\
  71 & 49 & 61\\
  43 & 61 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  7 & 9 & 6\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  2 & 8 & 2\\
  2 & 2 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 72 & 42\\
  72 & 98 & 62\\
  42 & 62 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  9 & 7 & 7\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 5\\
  1 & 9 & 3\\
  3 & 1 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 71 & 45\\
  91 & 79 & 73\\
  23 & 31 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  2 & 3 & 2\\
  6 & 8 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 3\\
  2 & 4 & 1\\
  2 & 9 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 72 & 43\\
  22 & 34 & 21\\
  62 & 89 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  2 & 3 & 2\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 5 & 6\\
  2 & 5 & 2\\
  2 & 5 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 75 & 46\\
  22 & 35 & 22\\
  22 & 35 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  2 & 3 & 2\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 8 & 3\\
  2 & 6 & 1\\
  2 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 78 & 43\\
  22 & 36 & 21\\
  22 & 41 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  2 & 3 & 2\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 5 & 3\\
  2 & 5 & 1\\
  2 & 5 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 75 & 43\\
  22 & 35 & 21\\
  42 & 65 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  2 & 3 & 2\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 8 & 6\\
  2 & 6 & 2\\
  2 & 1 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 78 & 46\\
  22 & 36 & 22\\
  12 & 21 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  2 & 3 & 2\\
  3 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 6\\
  2 & 4 & 2\\
  2 & 9 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 72 & 46\\
  22 & 34 & 22\\
  32 & 49 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  3 & 4 & 2\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 5 & 1\\
  1 & 5 & 3\\
  3 & 5 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 75 & 41\\
  31 & 45 & 23\\
  43 & 65 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  8 & 8 & 9\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 7\\
  2 & 9 & 1\\
  2 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 71 & 47\\
  82 & 89 & 91\\
  22 & 31 & 23\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  5 & 6 & 5\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 6\\
  2 & 8 & 2\\
  2 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 72 & 46\\
  52 & 68 & 52\\
  22 & 32 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 7 & 4\\
  5 & 6 & 5\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 3\\
  2 & 8 & 1\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 72 & 43\\
  52 & 68 & 51\\
  42 & 62 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 5 & 5\\
  6 & 4 & 4\\
  3 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  6 & 2 & 2\\
  3 & 1 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  85 & 55 & 55\\
  66 & 42 & 42\\
  33 & 21 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 5 & 5\\
  3 & 2 & 2\\
  6 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  3 & 1 & 1\\
  6 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  85 & 55 & 55\\
  33 & 21 & 21\\
  66 & 42 & 42\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  2 & 1 & 1\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 1\\
  1 & 7 & 1\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 53 & 31\\
  21 & 17 & 11\\
  52 & 54 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  2 & 1 & 1\\
  7 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 1\\
  1 & 6 & 1\\
  2 & 6 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 52 & 31\\
  21 & 16 & 11\\
  72 & 56 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  2 & 1 & 1\\
  9 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 5 & 1\\
  2 & 8 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 51 & 31\\
  21 & 15 & 11\\
  92 & 58 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  4 & 1 & 2\\
  7 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 8 & 1\\
  2 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 51 & 31\\
  41 & 18 & 21\\
  72 & 53 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  2 & 4 & 3\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 6\\
  2 & 6 & 3\\
  2 & 6 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 52 & 36\\
  22 & 46 & 33\\
  22 & 46 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  2 & 5 & 4\\
  3 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 4\\
  3 & 7 & 1\\
  1 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 53 & 34\\
  23 & 57 & 41\\
  31 & 74 & 47\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  3 & 2 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 1\\
  1 & 7 & 4\\
  2 & 4 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 53 & 31\\
  31 & 27 & 14\\
  22 & 24 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  4 & 7 & 6\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 6\\
  2 & 8 & 3\\
  2 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 51 & 36\\
  42 & 78 & 63\\
  22 & 43 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  1 & 2 & 2\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 7\\
  2 & 6 & 1\\
  2 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 52 & 37\\
  12 & 26 & 21\\
  22 & 46 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  1 & 2 & 2\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 9 & 7\\
  2 & 7 & 1\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 59 & 37\\
  12 & 27 & 21\\
  12 & 32 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  3 & 1 & 4\\
  2 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 4\\
  1 & 9 & 1\\
  3 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 52 & 34\\
  31 & 19 & 41\\
  23 & 51 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  4 & 5 & 6\\
  1 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 9\\
  1 & 9 & 3\\
  4 & 1 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 52 & 39\\
  41 & 59 & 63\\
  14 & 31 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  2 & 3 & 3\\
  1 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 9\\
  1 & 8 & 3\\
  4 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 54 & 39\\
  21 & 38 & 33\\
  14 & 32 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  3 & 6 & 4\\
  2 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 2\\
  2 & 8 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 54 & 32\\
  32 & 68 & 42\\
  22 & 52 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  1 & 3 & 2\\
  3 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 6 & 4\\
  3 & 4 & 1\\
  1 & 8 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 56 & 34\\
  13 & 34 & 21\\
  31 & 78 & 47\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 3\\
  2 & 3 & 4\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 7\\
  2 & 8 & 1\\
  2 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 51 & 37\\
  22 & 38 & 41\\
  22 & 43 & 36\\
\end{bmatrix}
```
