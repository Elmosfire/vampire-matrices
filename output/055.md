```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 3 & 3\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 2\\
  3 & 6 & 3\\
  1 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 44 & 42\\
  33 & 36 & 33\\
  21 & 22 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 3\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 6\\
  2 & 7 & 1\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 46\\
  22 & 17 & 31\\
  22 & 22 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 3\\
  4 & 6 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 6\\
  1 & 7 & 1\\
  6 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 42 & 46\\
  21 & 17 & 31\\
  46 & 62 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  7 & 9 & 9\\
  5 & 6 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 2\\
  5 & 5 & 5\\
  1 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 42 & 42\\
  75 & 95 & 95\\
  51 & 64 & 64\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  6 & 6 & 8\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 6\\
  2 & 8 & 4\\
  4 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 42 & 46\\
  62 & 68 & 84\\
  24 & 31 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 5\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 8\\
  4 & 6 & 2\\
  3 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 48\\
  34 & 46 & 52\\
  23 & 32 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 5\\
  4 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 4\\
  4 & 6 & 1\\
  3 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 44\\
  34 & 46 & 51\\
  43 & 62 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  5 & 6 & 6\\
  7 & 9 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 2\\
  1 & 4 & 4\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 42 & 42\\
  51 & 64 & 64\\
  75 & 95 & 95\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 4\\
  3 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 6 & 2\\
  3 & 6 & 2\\
  3 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 46 & 42\\
  33 & 46 & 42\\
  33 & 51 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 4\\
  3 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 4\\
  3 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 44 & 44\\
  33 & 44 & 44\\
  33 & 44 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 4\\
  6 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 2\\
  3 & 4 & 2\\
  3 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 44 & 42\\
  33 & 44 & 42\\
  63 & 84 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 4\\
  4 & 6 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 4\\
  1 & 4 & 4\\
  6 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 44\\
  31 & 44 & 44\\
  46 & 64 & 64\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  3 & 4 & 4\\
  5 & 9 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 6 & 2\\
  1 & 6 & 2\\
  6 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 46 & 42\\
  31 & 46 & 42\\
  56 & 91 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 2\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 4\\
  1 & 7 & 2\\
  3 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 44 & 44\\
  21 & 17 & 22\\
  23 & 31 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 2\\
  3 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 4\\
  1 & 6 & 2\\
  3 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 44\\
  21 & 16 & 22\\
  33 & 33 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 2\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 2\\
  2 & 6 & 1\\
  1 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 42 & 42\\
  22 & 16 & 21\\
  41 & 23 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 2\\
  2 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 4\\
  2 & 6 & 2\\
  1 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  39 & 42 & 44\\
  22 & 16 & 22\\
  21 & 13 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 2\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 2\\
  1 & 7 & 1\\
  3 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 44 & 42\\
  21 & 17 & 21\\
  43 & 61 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 1 & 2\\
  6 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  1 & 6 & 1\\
  3 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 42\\
  21 & 16 & 21\\
  63 & 63 & 58\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 3 & 4\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 4\\
  2 & 7 & 4\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 44\\
  42 & 37 & 44\\
  22 & 22 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 3 & 4\\
  4 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  2 & 7 & 2\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 42\\
  42 & 37 & 42\\
  42 & 42 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  5 & 7 & 7\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 4\\
  4 & 6 & 6\\
  3 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 44\\
  54 & 76 & 76\\
  23 & 32 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  5 & 7 & 7\\
  4 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 2\\
  4 & 6 & 3\\
  3 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 42\\
  54 & 76 & 73\\
  43 & 62 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 2 & 3\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 8\\
  2 & 6 & 2\\
  4 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 44 & 48\\
  22 & 26 & 32\\
  24 & 32 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 2 & 3\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 4\\
  3 & 6 & 1\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 44 & 44\\
  23 & 26 & 31\\
  21 & 22 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 2 & 3\\
  4 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 6 & 1\\
  4 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 44 & 44\\
  22 & 26 & 31\\
  44 & 62 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 5 & 6\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 7 & 2\\
  2 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 44 & 44\\
  44 & 57 & 62\\
  22 & 31 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 5 & 6\\
  3 & 5 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 4\\
  3 & 7 & 2\\
  4 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 44\\
  43 & 57 & 62\\
  34 & 51 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 5 & 6\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 2\\
  4 & 7 & 1\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 44 & 42\\
  44 & 57 & 61\\
  42 & 61 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 5 & 6\\
  4 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 4\\
  4 & 6 & 2\\
  2 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 42 & 44\\
  44 & 56 & 62\\
  42 & 53 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 5 & 6\\
  5 & 7 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 4\\
  3 & 6 & 2\\
  4 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 42 & 44\\
  43 & 56 & 62\\
  54 & 73 & 76\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 3 & 3\\
  2 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 8 & 4\\
  4 & 4 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 48 & 44\\
  24 & 34 & 32\\
  22 & 32 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 3 & 3\\
  3 & 5 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 8 & 4\\
  3 & 4 & 2\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 48 & 44\\
  23 & 34 & 32\\
  34 & 52 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 3 & 3\\
  6 & 8 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 6 & 2\\
  4 & 3 & 1\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 46 & 42\\
  24 & 33 & 31\\
  62 & 84 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 3 & 3\\
  4 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 8 & 2\\
  4 & 4 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 48 & 42\\
  24 & 34 & 31\\
  42 & 62 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 3 & 3\\
  4 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 2 & 2\\
  2 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 44 & 44\\
  24 & 32 & 32\\
  42 & 56 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 3 & 3\\
  5 & 7 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 4\\
  3 & 2 & 2\\
  4 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 44\\
  23 & 32 & 32\\
  54 & 76 & 76\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  5 & 4 & 4\\
  3 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  1 & 6 & 6\\
  3 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 42\\
  51 & 46 & 46\\
  33 & 33 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 2 & 2\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 4\\
  2 & 4 & 4\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 44 & 44\\
  22 & 24 & 24\\
  22 & 24 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 2 & 2\\
  2 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 6 & 2\\
  2 & 6 & 2\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 46 & 42\\
  22 & 26 & 22\\
  22 & 31 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 2 & 2\\
  4 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 4 & 2\\
  2 & 4 & 2\\
  2 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 44 & 42\\
  22 & 24 & 22\\
  42 & 44 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  2 & 2 & 2\\
  6 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  2 & 2 & 2\\
  2 & 7 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 42\\
  22 & 22 & 22\\
  62 & 57 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  6 & 7 & 8\\
  3 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 4\\
  3 & 7 & 4\\
  3 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 42 & 44\\
  63 & 77 & 84\\
  33 & 42 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  6 & 7 & 8\\
  6 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 2\\
  3 & 7 & 2\\
  3 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  33 & 42 & 42\\
  63 & 77 & 82\\
  63 & 82 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  6 & 7 & 8\\
  4 & 6 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 4\\
  1 & 7 & 4\\
  6 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 42 & 44\\
  61 & 77 & 84\\
  46 & 62 & 64\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 6 & 6\\
  3 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 4\\
  6 & 4 & 4\\
  1 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 44\\
  46 & 64 & 64\\
  31 & 44 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 6 & 6\\
  6 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 2\\
  6 & 4 & 2\\
  1 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 44 & 42\\
  46 & 64 & 62\\
  61 & 84 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  4 & 6 & 6\\
  2 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 6 & 2\\
  6 & 6 & 2\\
  1 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  31 & 46 & 42\\
  46 & 66 & 62\\
  21 & 31 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  6 & 5 & 6\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  3 & 8 & 3\\
  1 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 42\\
  63 & 58 & 63\\
  21 & 21 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 4 & 4\\
  6 & 5 & 5\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 2\\
  2 & 7 & 7\\
  2 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 42 & 42\\
  62 & 57 & 57\\
  22 & 22 & 22\\
\end{bmatrix}
```
