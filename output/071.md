```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  6 & 8 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 1\\
  4 & 7 & 1\\
  1 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 22 & 21\\
  24 & 37 & 41\\
  61 & 83 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  2 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 2\\
  3 & 8 & 2\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 23 & 22\\
  23 & 38 & 42\\
  22 & 42 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  3 & 7 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 7 & 2\\
  4 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 22 & 22\\
  21 & 37 & 42\\
  34 & 73 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  3 & 5 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 2\\
  3 & 7 & 2\\
  2 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 22 & 22\\
  23 & 37 & 42\\
  32 & 53 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  2 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 2\\
  1 & 8 & 2\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 23 & 22\\
  21 & 38 & 42\\
  24 & 62 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  2 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 8 & 2\\
  3 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 23 & 22\\
  22 & 38 & 42\\
  23 & 52 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  2 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 2\\
  4 & 8 & 2\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 23 & 22\\
  24 & 38 & 42\\
  21 & 32 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  2 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 1\\
  2 & 9 & 1\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 24 & 21\\
  22 & 39 & 41\\
  23 & 81 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  4 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 2\\
  4 & 6 & 2\\
  1 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 21 & 22\\
  24 & 36 & 42\\
  41 & 54 & 58\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  4 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 3 & 1\\
  3 & 8 & 1\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 23 & 21\\
  23 & 38 & 41\\
  42 & 82 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  4 & 8 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 6 & 2\\
  4 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 21 & 22\\
  21 & 36 & 42\\
  44 & 84 & 88\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  1 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 2\\
  1 & 9 & 2\\
  4 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 24 & 22\\
  21 & 39 & 42\\
  14 & 51 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  3 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  4 & 7 & 2\\
  1 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 22 & 22\\
  24 & 37 & 42\\
  31 & 43 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  3 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  2 & 7 & 2\\
  3 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 22 & 22\\
  22 & 37 & 42\\
  33 & 63 & 58\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 4\\
  4 & 6 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 2\\
  3 & 6 & 2\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 21 & 22\\
  23 & 36 & 42\\
  42 & 64 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 6\\
  4 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 2\\
  3 & 8 & 1\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 21 & 22\\
  23 & 38 & 61\\
  42 & 82 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 6\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 4\\
  4 & 8 & 2\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 21 & 24\\
  24 & 38 & 62\\
  21 & 32 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 6\\
  2 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 4\\
  3 & 8 & 2\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 21 & 24\\
  23 & 38 & 62\\
  22 & 42 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 6\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 2\\
  3 & 9 & 1\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 23 & 22\\
  23 & 39 & 61\\
  22 & 61 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 6\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 4\\
  3 & 9 & 2\\
  2 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 23 & 24\\
  23 & 39 & 62\\
  12 & 31 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 3 & 6\\
  4 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 2\\
  4 & 8 & 1\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 21 & 22\\
  24 & 38 & 61\\
  41 & 62 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 6 & 6\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 1\\
  2 & 8 & 4\\
  3 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 22 & 21\\
  42 & 68 & 64\\
  23 & 42 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 6 & 6\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 2\\
  2 & 8 & 8\\
  3 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 22 & 22\\
  42 & 68 & 68\\
  13 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 6 & 6\\
  6 & 9 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 1\\
  2 & 4 & 4\\
  3 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 21 & 21\\
  42 & 64 & 64\\
  63 & 96 & 96\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  3 & 6 & 6\\
  4 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 1\\
  3 & 6 & 3\\
  2 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 22 & 21\\
  33 & 66 & 63\\
  42 & 84 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  3 & 6 & 6\\
  2 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 2\\
  3 & 6 & 6\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 22 & 22\\
  33 & 66 & 66\\
  22 & 44 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  3 & 6 & 6\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 1\\
  3 & 9 & 3\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 23 & 21\\
  33 & 69 & 63\\
  12 & 31 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 4 & 4\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 1\\
  4 & 8 & 4\\
  1 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 22 & 21\\
  44 & 48 & 44\\
  21 & 22 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 4 & 4\\
  6 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  4 & 4 & 4\\
  1 & 6 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 21 & 21\\
  44 & 44 & 44\\
  61 & 56 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  7 & 6 & 6\\
  3 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  2 & 7 & 7\\
  3 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 21 & 21\\
  72 & 67 & 67\\
  33 & 33 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  3 & 3 & 9\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 7\\
  3 & 9 & 3\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 21 & 27\\
  33 & 39 & 93\\
  12 & 21 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 8 & 8\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 1\\
  4 & 8 & 4\\
  1 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 22 & 21\\
  44 & 88 & 84\\
  21 & 42 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 8 & 8\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 2\\
  4 & 8 & 8\\
  1 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 22 & 22\\
  44 & 88 & 88\\
  11 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 1 & 3\\
  3 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 3\\
  3 & 7 & 1\\
  2 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 21 & 23\\
  13 & 17 & 31\\
  32 & 43 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 1 & 3\\
  1 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 6\\
  2 & 8 & 2\\
  3 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 24 & 26\\
  12 & 18 & 32\\
  13 & 32 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 1 & 3\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 3\\
  3 & 8 & 1\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 24 & 23\\
  13 & 18 & 31\\
  22 & 42 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 1 & 3\\
  3 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  4 & 7 & 1\\
  1 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 21 & 23\\
  14 & 17 & 31\\
  31 & 23 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 1 & 3\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 6\\
  3 & 8 & 2\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 24 & 26\\
  13 & 18 & 32\\
  12 & 22 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 1 & 3\\
  3 & 6 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 3\\
  2 & 7 & 1\\
  3 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 21 & 23\\
  12 & 17 & 31\\
  33 & 63 & 69\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  1 & 1 & 3\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 3\\
  2 & 8 & 1\\
  3 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 24 & 23\\
  12 & 18 & 31\\
  23 & 62 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  6 & 5 & 6\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 2\\
  3 & 8 & 6\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 21 & 22\\
  63 & 58 & 66\\
  22 & 22 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  6 & 5 & 6\\
  4 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  3 & 8 & 3\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 21 & 21\\
  63 & 58 & 63\\
  42 & 42 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  4 & 8 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 2\\
  1 & 8 & 1\\
  4 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 21 & 22\\
  21 & 18 & 41\\
  44 & 82 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 4\\
  2 & 8 & 2\\
  3 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 21 & 24\\
  22 & 18 & 42\\
  23 & 32 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  3 & 8 & 2\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 21 & 24\\
  23 & 18 & 42\\
  22 & 22 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  1 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 4\\
  1 & 9 & 2\\
  4 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 23 & 24\\
  21 & 19 & 42\\
  14 & 41 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  2 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 4\\
  1 & 8 & 2\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 21 & 24\\
  21 & 18 & 42\\
  24 & 42 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 2\\
  2 & 9 & 1\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 23 & 22\\
  22 & 19 & 41\\
  23 & 61 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  4 & 8 & 1\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 21 & 22\\
  24 & 18 & 41\\
  41 & 22 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  2 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 4\\
  4 & 8 & 2\\
  1 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 21 & 24\\
  24 & 18 & 42\\
  21 & 12 & 18\\
\end{bmatrix}
```
