```math
\begin{bmatrix}
  4 & 3 & 4\\
  6 & 5 & 6\\
  5 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 2\\
  6 & 2 & 6\\
  4 & 3 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  42 & 34 & 42\\
  66 & 52 & 66\\
  54 & 43 & 54\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 4\\
  6 & 5 & 6\\
  7 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 1\\
  9 & 2 & 3\\
  1 & 3 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  43 & 34 & 41\\
  69 & 52 & 63\\
  71 & 53 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 3 & 4\\
  5 & 3 & 5\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 7\\
  5 & 5 & 5\\
  1 & 3 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  47 & 31 & 47\\
  55 & 35 & 55\\
  21 & 13 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 8\\
  4 & 3 & 6\\
  3 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 8\\
  6 & 1 & 2\\
  2 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  64 & 44 & 88\\
  46 & 31 & 62\\
  32 & 22 & 44\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 8\\
  4 & 3 & 6\\
  6 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 4\\
  6 & 1 & 1\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  64 & 44 & 84\\
  46 & 31 & 61\\
  62 & 42 & 77\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 8\\
  2 & 1 & 2\\
  3 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 4\\
  1 & 3 & 3\\
  2 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 44 & 84\\
  21 & 13 & 23\\
  32 & 21 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  1 & 3 & 1\\
  2 & 8 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 8 & 4\\
  1 & 7 & 1\\
  4 & 8 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 88 & 24\\
  11 & 37 & 11\\
  24 & 88 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  2 & 5 & 2\\
  1 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 4\\
  1 & 9 & 1\\
  4 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 86 & 24\\
  21 & 59 & 21\\
  14 & 61 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  2 & 1 & 2\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 8\\
  1 & 9 & 1\\
  2 & 3 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 82 & 28\\
  21 & 19 & 21\\
  12 & 23 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  2 & 3 & 2\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 6\\
  1 & 9 & 1\\
  3 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 84 & 26\\
  21 & 39 & 21\\
  13 & 42 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  3 & 7 & 2\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  1 & 9 & 2\\
  3 & 2 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 84 & 22\\
  31 & 79 & 22\\
  13 & 42 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  4 & 8 & 3\\
  3 & 9 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  1 & 9 & 1\\
  3 & 3 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 82 & 22\\
  41 & 89 & 31\\
  33 & 93 & 25\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  2 & 8 & 2\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 8 & 2\\
  2 & 8 & 2\\
  1 & 4 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 88 & 22\\
  22 & 88 & 22\\
  11 & 44 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 8 & 2\\
  1 & 2 & 1\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 8 & 6\\
  1 & 8 & 1\\
  3 & 4 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 88 & 26\\
  11 & 28 & 11\\
  13 & 44 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 7 & 6\\
  2 & 1 & 2\\
  3 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 7\\
  1 & 6 & 1\\
  3 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  67 & 72 & 67\\
  21 & 16 & 21\\
  33 & 33 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 7 & 6\\
  5 & 4 & 4\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  2 & 7 & 1\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 73 & 61\\
  52 & 47 & 41\\
  51 & 51 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 7 & 6\\
  5 & 4 & 4\\
  5 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 2\\
  1 & 7 & 2\\
  3 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  67 & 73 & 62\\
  51 & 47 & 42\\
  53 & 61 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 7 & 6\\
  5 & 4 & 4\\
  5 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  2 & 6 & 2\\
  1 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 72 & 62\\
  52 & 46 & 42\\
  51 & 43 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 7 & 6\\
  5 & 6 & 5\\
  5 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 2\\
  4 & 4 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  64 & 76 & 62\\
  54 & 64 & 52\\
  52 & 62 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 7\\
  2 & 1 & 3\\
  4 & 2 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 3\\
  1 & 1 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  37 & 22 & 73\\
  21 & 11 & 34\\
  42 & 22 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 7\\
  2 & 1 & 4\\
  4 & 2 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  2 & 2 & 3\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 21 & 74\\
  22 & 12 & 43\\
  42 & 22 & 78\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 2 & 7\\
  2 & 1 & 4\\
  2 & 1 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 8\\
  2 & 2 & 6\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  36 & 21 & 78\\
  22 & 12 & 46\\
  22 & 12 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 8 & 6\\
  5 & 3 & 4\\
  5 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  1 & 8 & 1\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 82 & 62\\
  51 & 38 & 41\\
  52 & 61 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 8 & 6\\
  3 & 3 & 2\\
  4 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 2\\
  1 & 3 & 4\\
  2 & 6 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 84 & 62\\
  31 & 33 & 24\\
  42 & 46 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 8 & 6\\
  5 & 6 & 5\\
  5 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  3 & 6 & 1\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 84 & 62\\
  53 & 66 & 51\\
  51 & 62 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 8 & 6\\
  5 & 6 & 5\\
  5 & 7 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 4\\
  2 & 6 & 2\\
  4 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  64 & 84 & 64\\
  52 & 66 & 52\\
  54 & 72 & 54\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 8 & 7\\
  4 & 3 & 3\\
  7 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  1 & 5 & 2\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 82 & 72\\
  41 & 35 & 32\\
  72 & 64 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 8 & 7\\
  2 & 1 & 2\\
  2 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 8\\
  1 & 6 & 1\\
  2 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 83 & 78\\
  21 & 16 & 21\\
  22 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 8 & 7\\
  5 & 6 & 5\\
  8 & 9 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 2\\
  4 & 2 & 2\\
  2 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  74 & 86 & 72\\
  54 & 62 & 52\\
  82 & 94 & 76\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  3 & 7 & 8\\
  2 & 4 & 6\\
  2 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 9\\
  2 & 6 & 2\\
  1 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  34 & 72 & 89\\
  22 & 46 & 62\\
  21 & 43 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  5 & 8 & 3\\
  3 & 7 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 4\\
  2 & 9 & 1\\
  7 & 4 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 51 & 14\\
  52 & 89 & 31\\
  37 & 74 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  4 & 5 & 2\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 3\\
  2 & 9 & 1\\
  4 & 3 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 52 & 13\\
  42 & 59 & 21\\
  24 & 43 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  2 & 5 & 1\\
  2 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 8 & 1\\
  2 & 8 & 1\\
  8 & 2 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 58 & 11\\
  22 & 58 & 11\\
  28 & 82 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  2 & 5 & 1\\
  3 & 7 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 7 & 1\\
  3 & 7 & 1\\
  2 & 8 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 57 & 11\\
  23 & 57 & 11\\
  32 & 78 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  2 & 4 & 1\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 2\\
  2 & 8 & 1\\
  6 & 4 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 56 & 12\\
  22 & 48 & 11\\
  26 & 64 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  6 & 1 & 1\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 9 & 1\\
  6 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 51 & 11\\
  61 & 19 & 11\\
  56 & 54 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  2 & 3 & 1\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 3\\
  2 & 8 & 1\\
  4 & 6 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 54 & 13\\
  22 & 38 & 11\\
  24 & 46 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  3 & 2 & 1\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 2\\
  1 & 9 & 1\\
  8 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 53 & 12\\
  31 & 29 & 11\\
  28 & 62 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  3 & 2 & 1\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 2\\
  2 & 8 & 1\\
  1 & 9 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 51 & 12\\
  32 & 28 & 11\\
  41 & 29 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  3 & 2 & 1\\
  5 & 7 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 2\\
  1 & 8 & 1\\
  8 & 9 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 51 & 12\\
  31 & 28 & 11\\
  58 & 79 & 23\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  4 & 3 & 2\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 4\\
  2 & 9 & 1\\
  2 & 4 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 51 & 14\\
  42 & 39 & 21\\
  22 & 24 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  3 & 7 & 2\\
  3 & 7 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 4\\
  3 & 8 & 1\\
  3 & 8 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 52 & 14\\
  33 & 78 & 21\\
  33 & 78 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  3 & 4 & 1\\
  4 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 1\\
  2 & 8 & 1\\
  3 & 7 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 53 & 11\\
  32 & 48 & 11\\
  43 & 67 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  3 & 4 & 1\\
  2 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 1\\
  1 & 9 & 1\\
  9 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 54 & 11\\
  31 & 49 & 11\\
  29 & 81 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  4 & 7 & 2\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 2\\
  2 & 9 & 1\\
  6 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 53 & 12\\
  42 & 79 & 21\\
  26 & 62 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  2 & 2 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 4\\
  2 & 8 & 1\\
  2 & 8 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 52 & 14\\
  22 & 28 & 11\\
  22 & 28 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  7 & 6 & 2\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  2 & 9 & 1\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 51 & 11\\
  72 & 69 & 21\\
  52 & 54 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 5 & 1\\
  4 & 9 & 2\\
  2 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 1\\
  2 & 9 & 1\\
  8 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 54 & 11\\
  42 & 99 & 21\\
  28 & 81 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 9 & 6\\
  9 & 9 & 7\\
  6 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 3 & 3\\
  3 & 7 & 1\\
  3 & 1 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  85 & 93 & 63\\
  93 & 97 & 71\\
  63 & 71 & 45\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  8 & 4 & 5\\
  8 & 2 & 5\\
  3 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 3\\
  3 & 7 & 1\\
  1 & 1 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  89 & 41 & 53\\
  83 & 27 & 51\\
  31 & 11 & 15\\
\end{bmatrix}
```
