```math
\begin{bmatrix}
  1 & 1 & 2\\
  5 & 2 & 7\\
  5 & 4 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 1 & 1\\
  4 & 8 & 2\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  15 & 11 & 21\\
  54 & 28 & 72\\
  53 & 41 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  5 & 2 & 7\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 1\\
  2 & 8 & 2\\
  4 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 11 & 21\\
  52 & 28 & 72\\
  54 & 51 & 69\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  5 & 2 & 7\\
  5 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  6 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 21\\
  56 & 28 & 72\\
  52 & 31 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 8 & 2\\
  2 & 8 & 2\\
  4 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 18 & 22\\
  12 & 18 & 22\\
  14 & 41 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  3 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 2\\
  2 & 4 & 2\\
  4 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 14 & 22\\
  12 & 14 & 22\\
  34 & 43 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 6 & 2\\
  2 & 6 & 2\\
  4 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 16 & 22\\
  12 & 16 & 22\\
  24 & 42 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 6 & 4\\
  2 & 6 & 4\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 16 & 24\\
  12 & 16 & 24\\
  14 & 22 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  4 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  8 & 2 & 2\\
  1 & 4 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 12 & 22\\
  18 & 12 & 22\\
  41 & 14 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  3 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 2\\
  4 & 4 & 2\\
  3 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 14 & 22\\
  14 & 14 & 22\\
  33 & 33 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  2 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 2\\
  4 & 6 & 2\\
  3 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 16 & 22\\
  14 & 16 & 22\\
  23 & 32 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  4 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  2 & 2 & 2\\
  4 & 4 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 12 & 22\\
  12 & 12 & 22\\
  44 & 44 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 4\\
  6 & 2 & 4\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 12 & 24\\
  16 & 12 & 24\\
  22 & 14 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  3 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  6 & 4 & 2\\
  2 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 14 & 22\\
  16 & 14 & 22\\
  32 & 23 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  6 & 2 & 2\\
  2 & 4 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 12 & 22\\
  16 & 12 & 22\\
  42 & 24 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  2 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 4\\
  2 & 2 & 4\\
  4 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 12 & 24\\
  12 & 12 & 24\\
  24 & 24 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 2\\
  4 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  4 & 2 & 2\\
  3 & 4 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 12 & 22\\
  14 & 12 & 22\\
  43 & 34 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  6 & 3 & 6\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  6 & 6 & 6\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 21\\
  66 & 36 & 66\\
  42 & 22 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  4 & 2 & 4\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 1\\
  4 & 8 & 4\\
  3 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 12 & 21\\
  44 & 28 & 44\\
  23 & 21 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  4 & 2 & 4\\
  3 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 2\\
  8 & 4 & 8\\
  1 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 11 & 22\\
  48 & 24 & 48\\
  31 & 13 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  4 & 2 & 4\\
  6 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  4 & 4 & 4\\
  3 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 21\\
  44 & 24 & 44\\
  63 & 33 & 58\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  4 & 2 & 4\\
  6 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 1\\
  8 & 4 & 4\\
  1 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 11 & 21\\
  48 & 24 & 44\\
  61 & 23 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 6\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 3\\
  4 & 8 & 2\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 12 & 23\\
  24 & 18 & 62\\
  23 & 31 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 6\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 3\\
  6 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 12 & 23\\
  26 & 18 & 62\\
  22 & 21 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 2 & 6\\
  2 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 2\\
  6 & 8 & 2\\
  2 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 13 & 22\\
  26 & 28 & 62\\
  22 & 31 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 2 & 6\\
  4 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 2\\
  6 & 6 & 2\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 11 & 22\\
  26 & 26 & 62\\
  42 & 42 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 2 & 6\\
  2 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 4\\
  6 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 11 & 24\\
  26 & 26 & 64\\
  22 & 22 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 2 & 6\\
  4 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 2\\
  8 & 6 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 11 & 22\\
  28 & 26 & 62\\
  41 & 32 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 3\\
  3 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 3\\
  6 & 4 & 2\\
  2 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 11 & 23\\
  16 & 14 & 32\\
  32 & 23 & 49\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 3\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 3\\
  4 & 6 & 2\\
  3 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 14 & 23\\
  14 & 16 & 32\\
  23 & 32 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 3\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 3\\
  6 & 6 & 2\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 14 & 23\\
  16 & 16 & 32\\
  22 & 22 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 3\\
  3 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 3\\
  8 & 4 & 2\\
  1 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 11 & 23\\
  18 & 14 & 32\\
  31 & 13 & 29\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  1 & 1 & 3\\
  3 & 3 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 3\\
  4 & 4 & 2\\
  3 & 3 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 11 & 23\\
  14 & 14 & 32\\
  33 & 33 & 69\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  2 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 8 & 2\\
  4 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 13 & 22\\
  22 & 18 & 42\\
  24 & 41 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  1 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 3 & 4\\
  2 & 8 & 4\\
  4 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 13 & 24\\
  22 & 18 & 44\\
  14 & 21 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  4 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 2\\
  8 & 6 & 2\\
  1 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 11 & 22\\
  28 & 16 & 42\\
  41 & 12 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  2 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 2\\
  4 & 8 & 2\\
  3 & 1 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 13 & 22\\
  24 & 18 & 42\\
  23 & 31 & 19\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  4 & 4 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 2\\
  2 & 6 & 2\\
  4 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 11 & 22\\
  22 & 16 & 42\\
  44 & 42 & 79\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  2 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 4\\
  6 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 24\\
  26 & 16 & 44\\
  22 & 12 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  4 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 2\\
  6 & 6 & 2\\
  2 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 11 & 22\\
  26 & 16 & 42\\
  42 & 22 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  2 & 2 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 4\\
  2 & 6 & 4\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 11 & 24\\
  22 & 16 & 44\\
  24 & 22 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 1 & 4\\
  4 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 2\\
  4 & 6 & 2\\
  3 & 2 & 9\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 11 & 22\\
  24 & 16 & 42\\
  43 & 32 & 59\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 2 & 5\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 3\\
  6 & 6 & 4\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 12 & 23\\
  26 & 26 & 54\\
  22 & 22 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  2 & 2 & 5\\
  1 & 1 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 6\\
  6 & 6 & 8\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 12 & 26\\
  26 & 26 & 58\\
  12 & 12 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 1 & 2\\
  3 & 3 & 6\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  6 & 6 & 6\\
  2 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 12 & 22\\
  36 & 36 & 66\\
  22 & 22 & 37\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 5 & 5\\
  2 & 1 & 1\\
  7 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 4 & 1\\
  1 & 4 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  69 & 54 & 51\\
  21 & 14 & 11\\
  72 & 52 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 5 & 5\\
  2 & 1 & 1\\
  9 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 3 & 1\\
  1 & 3 & 1\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  69 & 53 & 51\\
  21 & 13 & 11\\
  92 & 54 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 5 & 5\\
  6 & 4 & 4\\
  7 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 1\\
  2 & 6 & 2\\
  3 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  67 & 53 & 51\\
  62 & 46 & 42\\
  73 & 61 & 47\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 5 & 5\\
  5 & 3 & 3\\
  5 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  2 & 4 & 4\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 52 & 52\\
  52 & 34 & 34\\
  52 & 34 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 5 & 5\\
  5 & 3 & 3\\
  5 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 3 & 1\\
  2 & 6 & 2\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 53 & 51\\
  52 & 36 & 32\\
  52 & 41 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 5 & 5\\
  4 & 2 & 2\\
  7 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 1\\
  2 & 4 & 2\\
  1 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  69 & 52 & 51\\
  42 & 24 & 22\\
  71 & 34 & 27\\
\end{bmatrix}
```
