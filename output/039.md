```math
\begin{bmatrix}
  1 & 8 & 1\\
  2 & 1 & 1\\
  2 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 4\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 81 & 14\\
  21 & 19 & 11\\
  22 & 28 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  5 & 4 & 1\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  19 & 81 & 11\\
  51 & 49 & 11\\
  52 & 58 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  2 & 5 & 1\\
  2 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 2\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 83 & 12\\
  21 & 59 & 11\\
  22 & 68 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  1 & 6 & 1\\
  1 & 7 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 7 & 3\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  13 & 87 & 13\\
  11 & 69 & 11\\
  12 & 78 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  2 & 3 & 1\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 3\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  18 & 82 & 13\\
  21 & 39 & 11\\
  22 & 48 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  1 & 4 & 1\\
  1 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 5 & 5\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  15 & 85 & 15\\
  11 & 49 & 11\\
  12 & 58 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  1 & 5 & 1\\
  1 & 6 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 6 & 4\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  14 & 86 & 14\\
  11 & 59 & 11\\
  12 & 68 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  1 & 3 & 1\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 6\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 84 & 16\\
  11 & 39 & 11\\
  12 & 48 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  1 & 7 & 1\\
  1 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 8 & 2\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 88 & 12\\
  11 & 79 & 11\\
  12 & 88 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  2 & 7 & 1\\
  2 & 8 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 1\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  16 & 84 & 11\\
  21 & 79 & 11\\
  22 & 88 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  1 & 8 & 1\\
  1 & 9 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 9 & 1\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  11 & 89 & 11\\
  11 & 89 & 11\\
  12 & 98 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  1 & 8 & 1\\
  1 & 2 & 1\\
  1 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 7\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  17 & 83 & 17\\
  11 & 29 & 11\\
  12 & 38 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  6 & 1 & 2\\
  3 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 3\\
  3 & 7 & 1\\
  3 & 2 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  69 & 31 & 23\\
  63 & 17 & 21\\
  33 & 12 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  4 & 2 & 1\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 1\\
  4 & 2 & 3\\
  4 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 34 & 21\\
  44 & 22 & 13\\
  44 & 22 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  6 & 3 & 2\\
  6 & 3 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 2\\
  6 & 3 & 2\\
  6 & 3 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 33 & 22\\
  66 & 33 & 22\\
  66 & 33 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  6 & 3 & 2\\
  9 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  6 & 2 & 2\\
  6 & 7 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 32 & 22\\
  66 & 32 & 22\\
  96 & 47 & 32\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  6 & 3 & 2\\
  7 & 3 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 3 & 1\\
  7 & 3 & 1\\
  2 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  67 & 33 & 21\\
  67 & 33 & 21\\
  72 & 33 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  8 & 4 & 3\\
  8 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 3\\
  8 & 4 & 1\\
  8 & 4 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  64 & 32 & 23\\
  88 & 44 & 31\\
  88 & 44 & 31\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  8 & 3 & 2\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 1\\
  4 & 6 & 3\\
  4 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 32 & 21\\
  84 & 36 & 23\\
  44 & 21 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  3 & 1 & 1\\
  3 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 2 & 3\\
  3 & 4 & 1\\
  3 & 4 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  69 & 32 & 23\\
  33 & 14 & 11\\
  33 & 14 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 3 & 2\\
  9 & 5 & 3\\
  9 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 1\\
  9 & 2 & 3\\
  9 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  63 & 34 & 21\\
  99 & 52 & 33\\
  99 & 52 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  4 & 3 & 6\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 9\\
  2 & 7 & 3\\
  2 & 2 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 51 & 69\\
  42 & 37 & 63\\
  22 & 22 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  9 & 6 & 8\\
  7 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 1\\
  2 & 7 & 3\\
  1 & 2 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  48 & 51 & 61\\
  92 & 67 & 83\\
  71 & 52 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  3 & 4 & 4\\
  2 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 8 & 2\\
  2 & 4 & 6\\
  3 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 58 & 62\\
  32 & 44 & 46\\
  23 & 31 & 34\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  3 & 4 & 4\\
  7 & 9 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 7 & 1\\
  2 & 1 & 3\\
  3 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 57 & 61\\
  32 & 41 & 43\\
  73 & 94 & 97\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  3 & 4 & 4\\
  4 & 6 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 8 & 1\\
  2 & 4 & 3\\
  3 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  44 & 58 & 61\\
  32 & 44 & 43\\
  43 & 61 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  3 & 3 & 4\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 8\\
  2 & 4 & 6\\
  2 & 4 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 52 & 68\\
  32 & 34 & 46\\
  22 & 24 & 31\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  6 & 6 & 7\\
  4 & 5 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 3 & 1\\
  2 & 7 & 3\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 53 & 61\\
  62 & 67 & 73\\
  42 & 51 & 47\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  6 & 6 & 7\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  2 & 4 & 6\\
  2 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 52 & 62\\
  62 & 64 & 76\\
  52 & 54 & 64\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  2 & 2 & 3\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 9\\
  2 & 4 & 3\\
  2 & 4 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 52 & 69\\
  22 & 24 & 33\\
  22 & 24 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  4 & 5 & 6\\
  3 & 4 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 6\\
  2 & 4 & 6\\
  4 & 3 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  42 & 54 & 66\\
  42 & 54 & 66\\
  34 & 43 & 52\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  4 & 5 & 6\\
  5 & 6 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 6\\
  2 & 1 & 6\\
  4 & 7 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  42 & 51 & 66\\
  42 & 51 & 66\\
  54 & 67 & 82\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  4 & 5 & 6\\
  6 & 8 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 3\\
  2 & 4 & 3\\
  4 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  42 & 54 & 63\\
  42 & 54 & 63\\
  64 & 83 & 96\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  7 & 6 & 9\\
  5 & 5 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 3\\
  2 & 7 & 3\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 51 & 63\\
  72 & 67 & 93\\
  52 & 52 & 66\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  4 & 5 & 6\\
  6 & 5 & 8\\
  2 & 2 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 8\\
  2 & 7 & 6\\
  2 & 2 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  46 & 51 & 68\\
  62 & 57 & 86\\
  22 & 22 & 31\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 2\\
  6 & 3 & 2\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  4 & 6 & 1\\
  2 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 42 & 22\\
  64 & 36 & 21\\
  42 & 23 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 2\\
  6 & 3 & 2\\
  9 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  3 & 6 & 1\\
  9 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 42 & 22\\
  63 & 36 & 21\\
  99 & 63 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 2\\
  3 & 2 & 1\\
  4 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 4 & 2\\
  4 & 2 & 1\\
  2 & 6 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 44 & 22\\
  34 & 22 & 11\\
  42 & 26 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 2\\
  3 & 2 & 1\\
  9 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  3 & 2 & 1\\
  9 & 6 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 44 & 22\\
  33 & 22 & 11\\
  99 & 66 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 2\\
  6 & 4 & 2\\
  3 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 4 & 2\\
  6 & 4 & 2\\
  3 & 2 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 44 & 22\\
  66 & 44 & 22\\
  33 & 22 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 4 & 2\\
  7 & 3 & 2\\
  3 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  3 & 7 & 2\\
  4 & 1 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 42 & 22\\
  73 & 37 & 22\\
  34 & 21 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 2 & 2\\
  9 & 3 & 3\\
  6 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  9 & 3 & 3\\
  6 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 22 & 22\\
  99 & 33 & 33\\
  66 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 2 & 2\\
  4 & 1 & 1\\
  6 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  2 & 3 & 3\\
  8 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 22 & 22\\
  42 & 13 & 13\\
  68 & 22 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 2 & 2\\
  3 & 1 & 1\\
  7 & 2 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  4 & 1 & 1\\
  6 & 4 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 22 & 22\\
  34 & 11 & 11\\
  76 & 24 & 24\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 2 & 2\\
  7 & 2 & 2\\
  3 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  6 & 4 & 4\\
  4 & 1 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 22 & 22\\
  76 & 24 & 24\\
  34 & 11 & 11\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 2 & 2\\
  6 & 2 & 2\\
  9 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  6 & 2 & 2\\
  9 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  66 & 22 & 22\\
  66 & 22 & 22\\
  99 & 33 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  6 & 2 & 2\\
  6 & 2 & 2\\
  4 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 2\\
  8 & 2 & 2\\
  2 & 3 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  68 & 22 & 22\\
  68 & 22 & 22\\
  42 & 13 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 7 & 3\\
  3 & 3 & 1\\
  7 & 7 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 7 & 1\\
  1 & 1 & 3\\
  7 & 7 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  77 & 77 & 31\\
  31 & 31 & 13\\
  77 & 77 & 31\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 7 & 3\\
  9 & 4 & 2\\
  7 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 1\\
  1 & 7 & 3\\
  3 & 5 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  79 & 71 & 31\\
  91 & 47 & 23\\
  73 & 45 & 21\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 5 & 4\\
  4 & 2 & 2\\
  6 & 3 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 3\\
  2 & 4 & 2\\
  3 & 6 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  78 & 51 & 43\\
  42 & 24 & 22\\
  63 & 36 & 33\\
\end{bmatrix}
```
