```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 3 & 2\\
  3 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 6 & 2\\
  2 & 6 & 2\\
  6 & 3 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 36 & 22\\
  22 & 36 & 22\\
  36 & 63 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 3 & 2\\
  5 & 6 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 3 & 2\\
  4 & 3 & 2\\
  2 & 9 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 33 & 22\\
  24 & 33 & 22\\
  52 & 69 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 3 & 2\\
  3 & 7 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 7 & 1\\
  3 & 7 & 1\\
  4 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 37 & 21\\
  23 & 37 & 21\\
  34 & 71 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 3 & 2\\
  5 & 8 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 2\\
  2 & 4 & 2\\
  6 & 7 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 34 & 22\\
  22 & 34 & 22\\
  56 & 87 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 3 & 2\\
  4 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 2\\
  4 & 4 & 2\\
  2 & 7 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 34 & 22\\
  24 & 34 & 22\\
  42 & 57 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 3 & 2\\
  5 & 9 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 6 & 1\\
  3 & 6 & 1\\
  4 & 3 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 36 & 21\\
  23 & 36 & 21\\
  54 & 93 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  1 & 2 & 1\\
  8 & 2 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  2 & 1 & 2\\
  6 & 8 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 23 & 22\\
  12 & 12 & 12\\
  68 & 82 & 68\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  7 & 7 & 9\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 1 & 3\\
  4 & 9 & 1\\
  2 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 31 & 23\\
  74 & 79 & 91\\
  32 & 41 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  7 & 7 & 9\\
  4 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 1 & 3\\
  3 & 9 & 1\\
  5 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  21 & 31 & 23\\
  73 & 79 & 91\\
  45 & 71 & 47\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  5 & 5 & 5\\
  1 & 4 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 7 & 2\\
  2 & 7 & 4\\
  2 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 37 & 22\\
  25 & 75 & 45\\
  21 & 41 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  6 & 1 & 4\\
  1 & 1 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 1 & 6\\
  2 & 3 & 4\\
  3 & 1 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 13 & 26\\
  62 & 13 & 44\\
  31 & 11 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  9 & 8 & 8\\
  6 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 1 & 1\\
  3 & 9 & 1\\
  3 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  25 & 31 & 21\\
  93 & 89 & 81\\
  63 & 81 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  9 & 8 & 8\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  5 & 1 & 2\\
  3 & 9 & 2\\
  3 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  25 & 31 & 22\\
  93 & 89 & 82\\
  33 & 41 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  9 & 8 & 8\\
  4 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 2\\
  1 & 9 & 2\\
  7 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 31 & 22\\
  91 & 89 & 82\\
  47 & 71 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  9 & 8 & 8\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 1\\
  4 & 9 & 1\\
  1 & 1 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 31 & 21\\
  94 & 89 & 81\\
  51 & 51 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  3 & 5 & 3\\
  4 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 5 & 1\\
  3 & 5 & 3\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  21 & 35 & 21\\
  33 & 55 & 33\\
  45 & 75 & 45\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 6 & 4\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 1\\
  4 & 8 & 2\\
  3 & 1 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 34 & 12\\
  44 & 68 & 24\\
  23 & 41 & 12\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 6 & 4\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 2\\
  4 & 8 & 4\\
  3 & 1 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 34 & 22\\
  44 & 68 & 44\\
  13 & 21 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 6 & 4\\
  3 & 7 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 4 & 1\\
  2 & 8 & 2\\
  2 & 1 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 34 & 12\\
  24 & 68 & 24\\
  23 & 71 & 23\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 6 & 4\\
  5 & 7 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  4 & 4 & 4\\
  3 & 8 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 32 & 22\\
  44 & 64 & 44\\
  53 & 78 & 53\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  5 & 2 & 2\\
  3 & 1 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  9 & 1 & 2\\
  3 & 7 & 4\\
  1 & 4 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  29 & 31 & 22\\
  53 & 27 & 24\\
  31 & 14 & 13\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  7 & 8 & 6\\
  5 & 7 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 1 & 2\\
  1 & 7 & 4\\
  7 & 4 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  23 & 31 & 22\\
  71 & 87 & 64\\
  57 & 74 & 53\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 2 & 2\\
  6 & 1 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 5 & 4\\
  2 & 3 & 2\\
  5 & 8 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 35 & 24\\
  22 & 32 & 22\\
  56 & 81 & 56\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 2 & 2\\
  3 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 4\\
  2 & 6 & 2\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 32 & 24\\
  22 & 26 & 22\\
  35 & 45 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  6 & 5 & 4\\
  4 & 4 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 1 & 2\\
  2 & 7 & 4\\
  4 & 4 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 31 & 22\\
  62 & 57 & 44\\
  44 & 44 & 33\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  5 & 6 & 5\\
  6 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 1\\
  4 & 8 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 32 & 21\\
  54 & 68 & 51\\
  62 & 82 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  5 & 6 & 5\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 2 & 2\\
  4 & 8 & 2\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  24 & 32 & 22\\
  54 & 68 & 52\\
  32 & 42 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  5 & 6 & 5\\
  4 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 2 & 2\\
  2 & 8 & 2\\
  6 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 32 & 22\\
  52 & 68 & 52\\
  46 & 72 & 46\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  5 & 6 & 5\\
  2 & 5 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 2\\
  2 & 7 & 2\\
  6 & 4 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 31 & 22\\
  52 & 67 & 52\\
  26 & 45 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  7 & 3 & 2\\
  3 & 2 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 2 & 4\\
  4 & 3 & 6\\
  7 & 5 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  32 & 23 & 42\\
  47 & 33 & 62\\
  73 & 52 & 88\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  3 & 1 & 2\\
  1 & 1 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  8 & 2 & 6\\
  2 & 5 & 4\\
  3 & 2 & 1\\
\end{bmatrix} = 
\begin{bmatrix}
  28 & 23 & 26\\
  32 & 15 & 24\\
  31 & 21 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  7 & 8 & 7\\
  1 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 2\\
  2 & 3 & 2\\
  6 & 1 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 13 & 22\\
  72 & 38 & 72\\
  16 & 14 & 16\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  3 & 4 & 4\\
  2 & 4 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 3\\
  4 & 8 & 1\\
  3 & 1 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 34 & 23\\
  34 & 48 & 41\\
  23 & 41 & 17\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  3 & 4 & 4\\
  1 & 2 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 4 & 6\\
  4 & 8 & 2\\
  3 & 1 & 4\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 34 & 26\\
  34 & 48 & 42\\
  13 & 21 & 14\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  3 & 4 & 4\\
  5 & 7 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  2 & 1 & 3\\
  4 & 7 & 1\\
  3 & 4 & 7\\
\end{bmatrix} = 
\begin{bmatrix}
  22 & 31 & 23\\
  34 & 47 & 41\\
  53 & 74 & 57\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  1 & 5 & 5\\
  1 & 5 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 5 & 5\\
  2 & 5 & 3\\
  2 & 5 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 35 & 25\\
  21 & 55 & 35\\
  21 & 55 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 3 & 2\\
  5 & 5 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 1\\
  1 & 6 & 3\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 32 & 21\\
  41 & 36 & 23\\
  55 & 55 & 35\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  2 & 2 & 1\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 5 & 1\\
  1 & 5 & 3\\
  5 & 5 & 5\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 35 & 21\\
  21 & 25 & 13\\
  35 & 45 & 25\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 3 & 3\\
  6 & 8 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 1\\
  2 & 8 & 1\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 32 & 21\\
  42 & 38 & 31\\
  64 & 82 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 3 & 3\\
  3 & 4 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  6 & 2 & 2\\
  2 & 8 & 2\\
  4 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  26 & 32 & 22\\
  42 & 38 & 32\\
  34 & 42 & 26\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 3 & 3\\
  9 & 7 & 4\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 1 & 1\\
  3 & 7 & 1\\
  2 & 4 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 31 & 21\\
  43 & 37 & 31\\
  92 & 74 & 48\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  4 & 3 & 3\\
  5 & 5 & 1\\
\end{bmatrix} \times 
\begin{bmatrix}
  7 & 2 & 1\\
  3 & 8 & 1\\
  2 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  27 & 32 & 21\\
  43 & 38 & 31\\
  52 & 52 & 18\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  1 & 9 & 1\\
  1 & 4 & 6\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 8 & 2\\
  2 & 4 & 4\\
  2 & 5 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 38 & 22\\
  21 & 49 & 41\\
  21 & 54 & 36\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  1 & 9 & 1\\
  8 & 2 & 8\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 1\\
  2 & 1 & 2\\
  2 & 8 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 23 & 12\\
  21 & 19 & 21\\
  28 & 82 & 28\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  1 & 9 & 1\\
  2 & 8 & 2\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 8 & 1\\
  2 & 4 & 2\\
  2 & 5 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 38 & 12\\
  21 & 49 & 21\\
  22 & 58 & 22\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  1 & 9 & 1\\
  7 & 3 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 3 & 1\\
  2 & 3 & 2\\
  2 & 9 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 33 & 12\\
  21 & 39 & 21\\
  27 & 93 & 27\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  2 & 3 & 2\\
  1 & 9 & 1\\
  4 & 1 & 9\\
\end{bmatrix} \times 
\begin{bmatrix}
  1 & 2 & 2\\
  2 & 1 & 4\\
  2 & 8 & 3\\
\end{bmatrix} = 
\begin{bmatrix}
  12 & 23 & 22\\
  21 & 19 & 41\\
  24 & 81 & 39\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 5 & 8\\
  8 & 6 & 9\\
  2 & 6 & 3\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 1\\
  4 & 2 & 2\\
  4 & 2 & 8\\
\end{bmatrix} = 
\begin{bmatrix}
  73 & 54 & 81\\
  84 & 62 & 92\\
  42 & 26 & 38\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 5 & 8\\
  8 & 6 & 9\\
  7 & 5 & 7\\
\end{bmatrix} \times 
\begin{bmatrix}
  4 & 4 & 2\\
  6 & 2 & 4\\
  2 & 2 & 6\\
\end{bmatrix} = 
\begin{bmatrix}
  74 & 54 & 82\\
  86 & 62 & 94\\
  72 & 52 & 76\\
\end{bmatrix}
```
```math
\begin{bmatrix}
  7 & 5 & 8\\
  4 & 3 & 5\\
  4 & 3 & 5\\
\end{bmatrix} \times 
\begin{bmatrix}
  3 & 4 & 9\\
  4 & 2 & 2\\
  4 & 2 & 2\\
\end{bmatrix} = 
\begin{bmatrix}
  73 & 54 & 89\\
  44 & 32 & 52\\
  44 & 32 & 52\\
\end{bmatrix}
```
