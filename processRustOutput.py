import numpy as np

def bmatrix(a):
    if len(a.shape) > 2:
        raise ValueError('bmatrix can at most display two dimensions')
    lines = str(a).replace('[', '').replace(']', '').splitlines()
    rv = [r'\begin{bmatrix}']
    rv += ['  ' + ' & '.join(l.split()) + r'\\' for l in lines]
    rv +=  [r'\end{bmatrix}']
    return '\n'.join(rv)

def extract_rust_output(s):
    l,r = [[[int(y) for y in x.replace('0[','[').replace('1[','[').replace('2[','[').replace('[','').replace(']','').replace(' ','').split(',')] for x in sorted(l.split(';'))] for l in s.split('|')]
    left = np.array(l)
    right = np.array(r)
    return (left,right.T)

def export(core):
    with open(core + '.txt') as file:
        currentfile = None
        for i,l in enumerate(file):
            if (i%50) == 0:
                if currentfile is not None:
                    currentfile.close()
                currentfile = open(f'{core}\\{i//50:03}.md','w')
            left,right = extract_rust_output(l)
            #assert (left @ right == 10*left+right).all()
            currentfile.write('```math\n' + bmatrix(left) + ' \\times \n'  + bmatrix(right) + ' = \n' + bmatrix(left @ right) + '\n```\n')
        currentfile.close()

export('output')
export('output_mixed')
